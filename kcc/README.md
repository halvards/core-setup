# Config Connector setup

Not currently supported by Config Connector:

-   Cloud Endpoints
-   Google-managed TLS certificates
-   CA service
-   Workload certificates for GKE clusters

## Setup

1.  Increase Compute Engine
    [Backend services quota](https://console.cloud.google.com/iam-admin/quotas?pageState=(%22expandableQuotasTable%22:(%22f%22:%22%255B%257B_22k_22_3A_22Limit%2520name_22_2C_22t_22_3A10_2C_22v_22_3A_22_5C_22Backend%2520services_5C_22_22_2C_22s_22_3Atrue_2C_22i_22_3A_22displayName_22%257D%255D%22))) for *Location=Global* to 20.

2.  Set project ID envvar:

    ```sh
    export PROJECT_ID=$(gcloud config get-value core/project)
    ```

3.  Create VPC network:

    ```sh
    kubectl apply -f network.yaml
    ```

4.  Get the public IP address:

    ```sh
    export PUBLIC_IP=$(kubectl get computeaddress microsvcdemo-ext-vip \
        -o jsonpath='{.spec.address}')
    ```

5.  Create DNS A record using Cloud Endpoints:

    ```sh
    envsubst < cloud-endpoints.yaml.tmpl > cloud-endpoints.yaml

    # or, if you don't have `envsubst`:
    #eval "echo \"$(cat cloud-endpoints.yaml.tmpl)\"" > cloud-endpoints.yaml

    gcloud endpoints services deploy cloud-endpoints.yaml
    ```

6.  Create a Google-managed TLS certificate:

    ```sh
    gcloud compute ssl-certificates create microsvcdemo-ext-cert \
        --global --domains="microsvcdemo.endpoints.${PROJECT_ID}.cloud.goog"
    ```

    Config Connector issue tracking Google-managed TLS certificate support:
    [GoogleCloudPlatform/k8s-config-connector#107](https://github.com/GoogleCloudPlatform/k8s-config-connector/issues/107).

7.  After creating GKE clusters, enable workload certificates:

    ```sh
    gcloud beta container clusters update microsvcdemo-us-central1-c \
        --zone us-central1-c --enable-workload-certificates

    gcloud beta container clusters update microsvcdemo-asia-southeast1-b \
        --zone asia-southeast1-b --enable-workload-certificates
    ```

## Set up Config Connector

2.  Enable Google Cloud APIs:

    ```sh
    gcloud services enable \
        cloudresourcemanager.googleapis.com \
        serviceusage.googleapis.com
    ```

3.  Create environment variables:

    ```sh
    export PROJECT_ID=$(gcloud config get-value core/project)
    ```

4.  Create a GKE cluster with the Config Connector add-on.

    Follow namespace-scoped Config Connector installation instructions.
