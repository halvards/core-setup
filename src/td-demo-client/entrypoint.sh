#!/bin/sh

# Setup exit handling
trap 'kill -TERM $MAIN_PID; kill -TERM $HELPER_PID' TERM INT

# Move ports
export CLIENT_PORT=$PORT
export PORT=$(($PORT+1))
export ASPNETCORE_URLS="http://*:$PORT"

# Start real app
$1 $2 &
MAIN_PID=$!

# Check platform
TARGET=$(test -f /etc/alpine-release && echo 'td-demo-client-musl-x64')
TARGET=${TARGET:-td-demo-client-x64}

# Download demo client
wget -O ./td-demo-client "https://storage.googleapis.com/traffic-director-hipster-dev-assets/$TARGET" && chmod +x ./td-demo-client

# Start demo client
./td-demo-client > /tmp/td-demo-client.log 2>&1 &
HELPER_PID=$!

# Wait for both to complete (or end from trap)
wait $MAIN_PID

kill -TERM $HELPER_PID 2> /dev/null || :
wait $HELPER_PID

trap - TERM INT
wait $MAIN_PID
EXIT_STATUS=$?
wait $HELPER_PID