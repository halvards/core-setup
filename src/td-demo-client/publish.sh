#!/bin/bash

gsutil -h "Cache-Control:no-cache,max-age=0" cp ./entrypoint.sh gs://traffic-director-hipster-dev-assets/entrypoint.sh
gsutil -h "Cache-Control:no-cache,max-age=0" cp ./td-demo-client-musl-x64 gs://traffic-director-hipster-dev-assets/td-demo-client-musl-x64
gsutil -h "Cache-Control:no-cache,max-age=0" cp ./td-demo-client-x64 gs://traffic-director-hipster-dev-assets/td-demo-client-x64