module gitlab.com/gcp-traffic-director/core-setup/td-demo-client

go 1.15

require (
	cloud.google.com/go v0.74.0
	cloud.google.com/go/pubsub v1.3.1
	github.com/bradleyjkemp/grpc-tools v0.2.6
	github.com/golang/protobuf v1.4.3
	github.com/sirupsen/logrus v1.6.0
	go.opencensus.io v0.22.5
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
	google.golang.org/api v0.36.0
	google.golang.org/grpc v1.34.0
)
