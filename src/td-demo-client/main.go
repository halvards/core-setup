// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"cloud.google.com/go/compute/metadata"
	"cloud.google.com/go/pubsub"
	grpc_proxy "github.com/bradleyjkemp/grpc-tools/grpc-proxy"
	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2/google"

	"google.golang.org/api/compute/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	grpcmetadata "google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"go.opencensus.io/trace/propagation"
)

type serviceUpdateMessage struct {
	Enabled  bool
	Location string
}

const (
	listenPort                        = "25000"
	healthCheckPort                   = "54000"
	managementTopicName               = "managementservice"
	managementLoggingServiceTopicName = "managementloggingservice"
)

var log *logrus.Logger

func init() {
	log = logrus.New()
	log.Level = logrus.InfoLevel
	log.Formatter = &logrus.JSONFormatter{
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyTime:  "timestamp",
			logrus.FieldKeyLevel: "severity",
			logrus.FieldKeyMsg:   "message",
		},
		TimestampFormat: time.RFC3339Nano,
	}
	log.Out = os.Stdout
}

type tdDemoClientService struct {
	cancel           context.CancelFunc
	context          context.Context
	location         string
	loggingEnabled   bool
	loggingTopic     *pubsub.Topic
	projectID        string
	serviceName      string
	podID            string
	port             int
	serviceAvailable bool
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	log.Infof("starting to listen for signals")
	term := make(chan os.Signal, 1)
	signal.Notify(term, syscall.SIGTERM)

	svc := newService(ctx, cancel)

	grpc_proxy.RegisterDefaultFlags()
	flag.Set("interface", "0.0.0.0")
	flag.Set("destination", fmt.Sprintf("localhost:%v", svc.port+1))
	flag.Set("port", strconv.Itoa(svc.port))
	flag.Parse()

	srv := grpc.NewServer()

	done := make(chan bool)
	go svc.start(srv, done)

	go func() {
		<-term
		svc.stop()
		srv.Stop()
	}()

	<-done
	log.Info("program complete")
}

func newService(ctx context.Context, cancel context.CancelFunc) *tdDemoClientService {
	cs := new(tdDemoClientService)
	cs.cancel = cancel
	cs.context = ctx
	cs.loggingEnabled = true
	cs.serviceAvailable = true

	loggingEnabled := os.Getenv("CLIENT_LOGGING")
	if loggingEnabled != "" {
		cs.loggingEnabled, _ = strconv.ParseBool(loggingEnabled)
	}
	log.Infof("using logging enabled %v", cs.loggingEnabled)

	port := listenPort
	if os.Getenv("CLIENT_PORT") != "" {
		port = os.Getenv("CLIENT_PORT")
	}
	cs.port, _ = strconv.Atoi(port)
	log.Infof("using port %v", cs.port)

	cs.serviceName = os.Getenv("SERVICE_NAME")
	if cs.serviceName == "" {
		hostName := os.Getenv("HOSTNAME")
		parts := strings.Split(hostName, "-")
		cs.serviceName = parts[0]
	}
	log.Infof("using topic name %v", cs.serviceName)

	cs.podID = os.Getenv("POD_ID")
	if cs.podID == "" {
		cs.podID = os.Getenv("HOSTNAME")
	}
	log.Infof("using subscription name %v", cs.podID)

	cs.location = os.Getenv("SERVICE_LOCATION")
	if cs.location == "" {
		m := metadata.NewClient(&http.Client{})
		cs.location, _ = m.Zone()
	}
	log.Infof("using location name %v", cs.location)

	cs.projectID = os.Getenv("PROJECT_ID")
	if cs.projectID == "" {
		credentials, err := google.FindDefaultCredentials(cs.context, compute.ComputeScope)
		if err != nil {
			log.Errorf("failed to get credentials: %v", err)
		} else {
			cs.projectID = credentials.ProjectID
		}
	}
	log.Infof("using project %v", cs.projectID)

	return cs
}

func (cs *tdDemoClientService) start(srv *grpc.Server, done chan bool) {
	go cs.initializePubSub(done)
	go cs.startHealthCheck(srv)
	go cs.startProxy()
}

func (cs *tdDemoClientService) startProxy() {
	proxy, _ := grpc_proxy.New(
		grpc_proxy.DefaultFlags(),
		grpc_proxy.WithInterceptor(cs.intercept),
	)
	err := proxy.Start()
	if err != nil {
		log.Errorf("failed to start proxy: %v", err)
	}
}

func (cs *tdDemoClientService) startHealthCheck(srv *grpc.Server) {
	healthpb.RegisterHealthServer(srv, cs)

	l, err := net.Listen("tcp", fmt.Sprintf(":%v", healthCheckPort))
	if err == nil {
		log.Infof("starting hc service on port: %v", healthCheckPort)
		go srv.Serve(l)
	} else {
		log.Errorf("failed to open hc port: %v", err)
	}
}

func (cs *tdDemoClientService) stop() {
	cs.cancel()
}

func (cs *tdDemoClientService) initializePubSub(done chan bool) {
	log.Infof("starting to pull topic messages")

	pubSubClient, err := pubsub.NewClient(cs.context, cs.projectID)
	if err != nil {
		log.Errorf("could not create pubsub Client: %v", err)
		return
	}

	log.Infof("sending I'm alive notification [%s, %s]", cs.serviceName, cs.location)
	imAliveMsg := fmt.Sprintf("{name: \"%s\", location: \"%s\"}", cs.serviceName, cs.location)

	msTopic, err := cs.createTopicIfNotExists(cs.context, pubSubClient, managementTopicName)
	if err != nil {
		log.Errorf("failed to create the management service topic: %v", err)
		return
	}
	if err := cs.sendMsgToTopic(cs.context, msTopic, imAliveMsg); err != nil {
		log.Errorf("could not send alive notification: %v", err)
		return
	}

	cs.loggingTopic, err = cs.createTopicIfNotExists(cs.context, pubSubClient, managementLoggingServiceTopicName)
	if err != nil {
		log.Errorf("failed to create the management logging topic: %v", err)
		return
	}

	topic, err := cs.createTopicIfNotExists(cs.context, pubSubClient, cs.serviceName)
	if err != nil {
		log.Errorf("failed to create the service topic: %v", err)
		return
	}

	sub, err := cs.createTopicSubscriptionIfNotExists(pubSubClient, topic)
	if err != nil {
		log.Errorf("failed to create the topic subscription: %v", err)
		return
	}

	defer func() {
		ctx := context.Background()

		msTopic.Stop()
		cs.loggingTopic.Stop()
		topic.Stop()

		if err := cs.deleteTopicSubscriptionIfExists(ctx, sub); err != nil {
			log.Errorf("failed to clean up subscription: %v", err)
		}

		done <- true
	}()

	if err := cs.pullMsgs(pubSubClient, sub); err != nil {
		log.Errorf("failed to pull messages: %v", err)
	}
}

func (cs *tdDemoClientService) createTopicIfNotExists(ctx context.Context, c *pubsub.Client, topicID string) (*pubsub.Topic, error) {
	t := c.Topic(topicID)
	ok, err := t.Exists(ctx)
	if err != nil {
		log.Errorf("failed to check if the topic exists: %v", err)
		return nil, err
	}
	if ok {
		return t, nil
	}
	t, err = c.CreateTopic(ctx, topicID)
	if err != nil {
		log.Errorf("failed to create the topic: %v", err)
		return nil, err
	}
	return t, nil
}

func (cs *tdDemoClientService) createTopicSubscriptionIfNotExists(client *pubsub.Client, topic *pubsub.Topic) (*pubsub.Subscription, error) {
	sub := client.Subscription(cs.podID)
	ok, err := sub.Exists(cs.context)
	if err != nil {
		log.Errorf("failed to check if the subscription exists: %v", err)
		return nil, err
	}

	if !ok {
		sub, err := client.CreateSubscription(cs.context, cs.podID, pubsub.SubscriptionConfig{
			Topic:            topic,
			AckDeadline:      30 * time.Second,
			ExpirationPolicy: 24 * time.Hour,
		})
		if err != nil {
			log.Errorf("failed to create subscription: %v", err)
			return nil, err
		}
		log.Infof("created subscription: %v", sub)
	}

	return sub, nil
}

func (cs *tdDemoClientService) deleteTopicSubscriptionIfExists(ctx context.Context, sub *pubsub.Subscription) error {
	ok, err := sub.Exists(ctx)
	if err != nil {
		log.Errorf("failed to check if the subscription exists: %v", err)
		return err
	}

	if ok {
		err := sub.Delete(ctx)
		if err != nil {
			return err
		}
		log.Infof("deleted subscription: %v", sub)
	}

	return nil
}

func (cs *tdDemoClientService) pullMsgs(client *pubsub.Client, sub *pubsub.Subscription) error {
	err := sub.Receive(cs.context, func(ctx context.Context, msg *pubsub.Message) {
		msg.Ack()
		serviceUpdateMessage := serviceUpdateMessage{}
		json.Unmarshal(msg.Data, &serviceUpdateMessage)
		if cs.location == serviceUpdateMessage.Location {
			log.Infof("updating service availability with value: %v", serviceUpdateMessage.Enabled)
			cs.serviceAvailable = serviceUpdateMessage.Enabled
		}
	})

	if err != nil {
		return err
	}

	return nil
}

func (cs *tdDemoClientService) sendMsgToTopic(ctx context.Context, topic *pubsub.Topic, msg string) error {
	result := topic.Publish(ctx, &pubsub.Message{
		Data: []byte(msg),
	})
	_, err := result.Get(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (cs *tdDemoClientService) intercept(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	if strings.Contains("/grpc.health.v1.Health/Check", info.FullMethod) {
		status := healthpb.HealthCheckResponse_SERVING
		if !cs.serviceAvailable {
			status = healthpb.HealthCheckResponse_NOT_SERVING
		}

		healthCheckResponse := &healthpb.HealthCheckResponse{Status: status}
		log.Debugf("indirect health check response: %v", healthCheckResponse)

		msg, err := proto.Marshal(healthCheckResponse)
		if err != nil {
			log.Errorf("failed to marshal health check response: %v", err)
			return err
		}

		err = ss.SendMsg(msg)
		if err != nil {
			log.Errorf("failed to send health check response message: %v", err)
			return err
		}
	} else {
		if cs.loggingEnabled {
			cs.handleRequestID(ss)
		}

		err := handler(srv, ss)
		if err != nil {
			log.Errorf("rpc proxy call failed with error: %v", err)
			return err
		}
	}

	return nil
}

func (cs *tdDemoClientService) handleRequestID(ss grpc.ServerStream) error {
	md, ok := grpcmetadata.FromIncomingContext(ss.Context())
	ctx := context.Background()
	if ok {
		trace, ok := md["grpc-trace-bin"]
		if ok {
			traceBin := []byte(trace[0])
			parent, ok := propagation.FromBinary(traceBin)
			if ok {
				certArray, ok := md["x-forwarded-client-cert"]
				if !ok || len(certArray) == 0 {
					certArray = []string{""}
				}

				cert := strings.Replace(certArray[0], "\"", "\\\"", -1)

				traceMsg := fmt.Sprintf("{\"name\": \"%s\", \"location\": \"%s\", \"requestId\": \"%s\", \"podId\": \"%s\", \"certificate\": \"%s\"}", cs.serviceName, cs.location, parent.TraceID, cs.podID, cert)
				log.Debugf("publishing log: %v", traceMsg)

				if err := cs.sendMsgToTopic(ctx, cs.loggingTopic, traceMsg); err != nil {
					log.Errorf("could not send request notification: %v", err)
					return err
				}
			} else {
				log.Error("unable to deserialize trace header")
			}
		} else {
			log.Error("unable to find trace header")
		}
	} else {
		log.Error("unable to fetch metadata from incoming request")
	}

	return nil
}

func (cs *tdDemoClientService) Check(ctx context.Context, req *healthpb.HealthCheckRequest) (*healthpb.HealthCheckResponse, error) {
	status := healthpb.HealthCheckResponse_SERVING
	if !cs.serviceAvailable {
		status = healthpb.HealthCheckResponse_NOT_SERVING
	}

	log.Debugf("direct health check response: %v", status)
	return &healthpb.HealthCheckResponse{Status: status}, nil
}

func (cs *tdDemoClientService) Watch(req *healthpb.HealthCheckRequest, ws healthpb.Health_WatchServer) error {
	return status.Errorf(codes.Unimplemented, "health check via Watch not implemented")
}
