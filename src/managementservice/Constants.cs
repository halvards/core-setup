namespace managementservice
{
    public static class Constants
    {
        public static readonly string[] Services = new []
        {
            "adservice",
            "cartservice",
            "checkoutservice",
            "currencyservice",
            "emailservice",
            "paymentservice",
            "productcatalogservice",
            "recommendationservice",
            "shippingservice"
        };

        public static readonly string[] Locations = new []
        {
            "us-central1-c",
            "asia-southeast1-b"
        };

        public const string ManagementServiceTopicName = "managementservice";
        public const string ManagementLoggingServiceTopicName = "managementloggingservice";
        public const int MaxRequestInformationAvailabilityMin = 60;
    }
}
