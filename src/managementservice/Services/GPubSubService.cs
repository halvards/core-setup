﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Grpc.Core;
using managementservice.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace managementservice.Services
{
  public class GPubSubService
    {
        private readonly ILogger<GPubSubService> _logger;
        private readonly Lazy<string> _projectId;
        private Subscription _managementServiceSubscription;

        public GPubSubService(ILogger<GPubSubService> logger)
        {
            _logger = logger;
            _projectId = new Lazy<string>(() => 
            {
                var local = Environment.GetEnvironmentVariable("PROJECT_ID");
                if (!string.IsNullOrEmpty(local))
                {
                    return local;
                }
                
                return Google.Api.Gax.Platform.Instance().ProjectId;
            });
        }

        public async Task Initialize(string name)
        {
            var publisherService = await PublisherServiceApiClient.CreateAsync();
            var topicName = TopicName.FromProjectTopic(_projectId.Value, name);

            Topic topic = null;
            try
            {
                topic = publisherService.CreateTopic(topicName);
                _logger.LogInformation($"Topic {name} created");
            }
            catch (RpcException e) when (e.Status.StatusCode == StatusCode.AlreadyExists)
            {
                _logger.LogInformation($"Topic {name} already exists");
            }
        }

        public async Task InitializeManagementServiceTopicSubscription(Func<ImAliveServiceMessage, Task> action)
        {
            _managementServiceSubscription = await CreateSubscription(Constants.ManagementServiceTopicName, Constants.ManagementServiceTopicName);
            var subscriber = await SubscriberClient.CreateAsync(_managementServiceSubscription.SubscriptionName);

            // In case of any error we will start the subscriber again to keep it always running
            while (true)
            {
                try
                {
                    var subscriberTask = subscriber.StartAsync(async (PubsubMessage message, CancellationToken cancel) =>
                    {
                        var text = Encoding.UTF8.GetString(message.Data.ToArray());
                        var imAliveServiceMessage = SafeDeserializeObject<ImAliveServiceMessage>(text);
                        if (imAliveServiceMessage == null || string.IsNullOrEmpty(imAliveServiceMessage.Name) || string.IsNullOrEmpty(imAliveServiceMessage.Location))
                        {
                            return SubscriberClient.Reply.Ack;
                        }

                        await action(imAliveServiceMessage);
                        return SubscriberClient.Reply.Ack;
                    });
                    await subscriberTask;
                }
                catch (Exception e)
                {
                    _logger.LogWarning("Error with management service subscription: {Exception}", e);
                }

            }
        }

        public async Task InitializeManagementLoggingServiceTopicSubscription(Func<ServiceLoggingRequestMessage, Task> action)
        {
            _managementServiceSubscription = await CreateSubscription(Constants.ManagementLoggingServiceTopicName, Constants.ManagementLoggingServiceTopicName);
            var subscriber = await SubscriberClient.CreateAsync(_managementServiceSubscription.SubscriptionName);

            // In case of any error we will start the subscriber again to keep it always running
            while (true)
            {
                try
                {
                    var subscriberTask = subscriber.StartAsync(async (PubsubMessage message, CancellationToken cancel) =>
                    {
                        var text = Encoding.UTF8.GetString(message.Data.ToArray());
                        var serviceLoggingRequestMessage = SafeDeserializeObject<ServiceLoggingRequestMessage>(text);
                        if (serviceLoggingRequestMessage == null
                            || string.IsNullOrEmpty(serviceLoggingRequestMessage.Name) 
                            || string.IsNullOrEmpty(serviceLoggingRequestMessage.Location)
                            || string.IsNullOrEmpty(serviceLoggingRequestMessage.RequestId)
                            || string.IsNullOrEmpty(serviceLoggingRequestMessage.PodId))
                        {
                            return SubscriberClient.Reply.Ack;
                        }

                        await action(serviceLoggingRequestMessage);
                        return SubscriberClient.Reply.Ack;
                    });
                    await subscriberTask;
                }
                catch (Exception e)
                {
                    _logger.LogWarning("Error with management logging service subscription: {Exception}", e);
                }
            }
        }

        public async Task Update(string name, string location, bool enabled)
        {
            _logger.LogInformation($"Sending service update for {name} in {location}: {enabled}");

            var topicName = new TopicName(_projectId.Value, name);
            var publisher = await PublisherClient.CreateAsync(topicName);
            var messageId = await publisher.PublishAsync(JsonConvert.SerializeObject(new { Enabled = enabled, Location = location }));
            _logger.LogInformation($"Send service update for {name}: {messageId}");
        }

        public async Task<Subscription> CreateSubscription(string topicId, string subscriptionId)
        {
            var subscriber = await SubscriberServiceApiClient.CreateAsync();
            var subscriptionName = SubscriptionName.FromProjectSubscription(_projectId.Value, subscriptionId);
            Subscription subscription = null;
            try
            {
                subscription = await subscriber.GetSubscriptionAsync(subscriptionName);
                return subscription;
            }
            catch (RpcException e) when (e.Status.StatusCode == StatusCode.NotFound)
            {
                // If not exists we will create it
            }

            try
            {
                var topicName = TopicName.FromProjectTopic(_projectId.Value, topicId);
                subscription = subscriber.CreateSubscription(subscriptionName, topicName, pushConfig: null, ackDeadlineSeconds: 60);
            }
            catch (Exception e)
            {
                _logger.LogError("Error creating the subscription", e);
            }
            return subscription;
        }

        private T SafeDeserializeObject<T>(string input)
            where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(input);
            }
            catch (JsonException e)
            {
                _logger.LogWarning("Failed to deserialize message \"{Message}\": {Error}", input, e);
                return null;
            }
        }
    }
}
