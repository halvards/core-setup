using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using managementservice.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace managementservice.Services
{
  public class LoggingService
    {
        private readonly ILogger<LoggingService> _logger;
        private readonly GPubSubService _gPubSubService;
        private readonly IMemoryCache _cache;
        private bool _isInitialized;

        public LoggingService(GPubSubService gPubSubService, IMemoryCache cache, ILogger<LoggingService> logger)
        {
            _logger = logger;
            _gPubSubService = gPubSubService;
            _cache = cache;
        }

        public RequestInfo GetRequestInformation(string requestId) =>
            _cache.TryGetValue<RequestInfo>(requestId, out var request) ? request : null;

        public void Initialize()
        {
            if (_isInitialized)
            {
              return;
            }

            _isInitialized = true;

#pragma warning disable CS4014 // Subscriber will keep running
            _gPubSubService.InitializeManagementLoggingServiceTopicSubscription((ServiceLoggingRequestMessage message) =>
            {
                LogServiceRequest(message);
                return Task.CompletedTask;
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private void LogServiceRequest(ServiceLoggingRequestMessage serviceLoggingRequestMessage)
        {
            if (string.IsNullOrEmpty(serviceLoggingRequestMessage.Name))
            {
                throw new ArgumentNullException(nameof(serviceLoggingRequestMessage.Name));
            }

            if (string.IsNullOrEmpty(serviceLoggingRequestMessage.Location))
            {
                throw new ArgumentNullException(nameof(serviceLoggingRequestMessage.Location));
            }

            if (string.IsNullOrEmpty(serviceLoggingRequestMessage.RequestId))
            {
                throw new ArgumentNullException(nameof(serviceLoggingRequestMessage.RequestId));
            }

            if (string.IsNullOrEmpty(serviceLoggingRequestMessage.PodId))
            {
                throw new ArgumentNullException(nameof(serviceLoggingRequestMessage.PodId));
            }

            var serviceInfo = new ServiceRequestInfo
            {
                Name = serviceLoggingRequestMessage.Name,
                Location = serviceLoggingRequestMessage.Location,
                PodId = serviceLoggingRequestMessage.PodId,
                Certificate = serviceLoggingRequestMessage.Certificate
            };

            if (!_cache.TryGetValue<RequestInfo>(serviceLoggingRequestMessage.RequestId, out var requestInfo))
            {
                requestInfo = new RequestInfo
                {
                    RequestId = serviceLoggingRequestMessage.RequestId,
                    Created = DateTime.Now,
                    ServicesInfo = new List<ServiceRequestInfo> { serviceInfo }
                };
                _cache.Set(serviceLoggingRequestMessage.RequestId, requestInfo,
                    new MemoryCacheEntryOptions
                    {
                        AbsoluteExpiration = DateTime.UtcNow.AddMinutes(15)
                    });
            }
            else
            {
                var addedServiceInfo = requestInfo.ServicesInfo.FirstOrDefault(serviceInfo =>
                    string.Equals(serviceInfo.Name, serviceLoggingRequestMessage.Name, StringComparison.OrdinalIgnoreCase));
                if (addedServiceInfo == null)
                {
                    // Only add the service information if it's not there yet.
                    requestInfo.ServicesInfo.Add(serviceInfo);
                }
            }
        }
    }
}