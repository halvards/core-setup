using System.Collections.Generic;
using System.Threading.Tasks;
using managementservice.Models;
using managementservice.Services;
using Microsoft.AspNetCore.Mvc;

namespace managementservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ControlController : ControllerBase
    {
        private readonly ControlService _controlService;

        public ControlController(ControlService controlService)
        {
            _controlService = controlService;
        }

        [HttpGet]
        public Task<List<ServiceModel>> GetAll()
        {
            return _controlService.GetServices();
        }

        [HttpPut]
        public Task Update([FromBody] ServiceUpdateModel model)
        {
            return _controlService.UpdateService(model.Name, model.Location, model.Enabled);
        }
    }
}

