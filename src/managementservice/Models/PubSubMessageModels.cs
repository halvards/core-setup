﻿namespace managementservice.Models
{
    public class ImAliveServiceMessage
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }

    public class ServiceLoggingRequestMessage
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string RequestId { get; set; }
        public string PodId { get; set; }
        public string Certificate { get; set; }
    }
}
