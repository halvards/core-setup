namespace managementservice.Models
{
    public class ServiceRequestInfo
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string PodId { get; set; }
        public string Certificate { get; set; }
    }
}