#!/bin/bash

PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
VERSION="0.2.2"
CONTAINER_VERSION="0.17.0"

if [ ! -f "./microservices-demo-${VERSION}.tar.gz" ]; then
  wget -O "./microservices-demo-${VERSION}.tar.gz" "https://github.com/GoogleCloudPlatform/microservices-demo/archive/v${VERSION}.tar.gz"
fi

if [ -d "./microservices-demo-${VERSION}" ]; then
  rm -rf "./microservices-demo-${VERSION}"
fi

tar zxf "./microservices-demo-${VERSION}.tar.gz"

cd "./microservices-demo-${VERSION}/src"

cd ./frontend

cp ../../../frontend/script.js ./static

sed -i "s/\"github\\.com\\/sirupsen\\/logrus\"/\"github\\.com\\/sirupsen\\/logrus\"\n\t\"go\\.opencensus\\.io\\/trace\"/" ./handlers.go
sed -i "s/\"request_id\":[ ]*r.Context()\\.Value(ctxKeyRequestID{}),/\"request_id\": trace\\.FromContext(r\\.Context()).SpanContext()\\.TraceID,/g" ./handlers.go

cp ./templates/footer.html ../footer.html
sed -i "s/<\\/body>/<script src=\"\\/static\\/script\\.js\"><\\/script>\n<\\/body>/" ./templates/footer.html

gcloud builds submit -t "gcr.io/${PROJECT_ID}/frontend:${CONTAINER_VERSION}" .

cp ./static/styles/styles.css ../styles.css
cat ../../../frontend/mobile.css >> ./static/styles/styles.css

gcloud builds submit -t "gcr.io/${PROJECT_ID}/frontend-mobile:${CONTAINER_VERSION}" .

mv ../footer.html ./templates/
mv ../styles.css ./static/styles/styles.css
cat ../../../frontend/internal.css >> ./static/styles/styles.css

gcloud builds submit -t "gcr.io/${PROJECT_ID}/frontend-internal:${CONTAINER_VERSION}" .

cd ../shippingservice

SHIPPING_CALC=$(cat ./quote.go | tr '\n' '\r' | sed "s/\tcount64 := float64(count)\r\tvar p = 1 + (count64 \\* 0\\.2)\r\treturn count64 + math\\.Pow(3, p)/\treturn 1.23/" | tr '\r' '\n')
echo "$SHIPPING_CALC" > ./quote.go

gcloud builds submit -t "gcr.io/${PROJECT_ID}/shippingservice-enhanced:${CONTAINER_VERSION}" .

cd ../../..

rm -rf "./microservices-demo-${VERSION}"