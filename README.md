# Traffic Director Microservices Demo Lab Guide

- [Introduction](#introduction)
  - [Architecture Overview](#architecture-overview)
  - [Prerequisites](#prerequisites)
- [Setup Options](#setup-options)
  - [Option 1: Manual setup](#option-1-manual-setup)
  - [Option 2: Automated script setup](#option-2-automated-script-setup)
- [Cleaning up](#cleaning-up)
- [Demo implementation](#demo-implementation)

## Introduction

[Traffic Director](https://cloud.google.com/traffic-director) is Google Cloud's
fully managed service mesh.

Use this lab to learn about how Traffic Director enables the following service
mesh capabilities:

- Global load balancing and health checking for multi-regional services and
  automatic failover
- Canary (or blue/green) deployments for safe rollouts
- "Zero Trust" security (enabled by authentication, encryption and authorization)
- Support for virtual machines and containers

## Architecture Overview

The deployment described in this lab spans across two Google Cloud regions:

- Each Cloud region hosts the same set of services.
- All services* are hosted as containers on Google Kubernetes Engine. There are
  two clusters (one per region) and each cluster hosts an identical set of
  services.
- These services are configured as global services in Traffic Director. These
  global services have serving capacity in both regions, which enables high
  availability using an active/active setup.
- The `FrontEnd` service is implemented as an HTTP application while all other
  services are implemented as gRPC applications. These applications use Envoy
  sidecars as their service mesh infrastructure.
- The `ShippingService` is also hosted on a Google Compute Engine virtual
  machine. This setup is used when demonstrating Traffic Director's blue/green
  (or "canary") deployments functionality.

The following diagram showcases the different services that make up the
Microservices Demo:

![Microservices-demo](https://github.com/GoogleCloudPlatform/microservices-demo/raw/master/docs/img/architecture-diagram.png)

This demo includes a management service user interface which you can use to
trigger events to support any customer-facing demos.

## Prerequisites

1. Ensure that you have a [GCP project](https://cloud.google.com/resource-manager/docs/creating-managing-projects#creating_a_project)
with [billing](https://cloud.google.com/billing/docs/how-to/modify-project)
enabled.

1. If you're using [Cloud Shell](https://cloud.google.com/shell), most of the
prerequisites for this lab are pre-installed. If you're using a different
environment, install [gcloud SDK](https://cloud.google.com/sdk/install),
[wget](https://www.gnu.org/software/wget/), and
[jq](https://stedolan.github.io/jq/).

1. Ensure the Google Cloud SDK version you are using is version 320.0.0, or later

  ```shell
  gcloud version --format="value('Google Cloud SDK')"
  ```

1. Enable APIs:

  ```shell
  gcloud services enable compute.googleapis.com \
    cloudprofiler.googleapis.com \
    cloudresourcemanager.googleapis.com \
    cloudtrace.googleapis.com \
    container.googleapis.com \
    gkehub.googleapis.com \
    networksecurity.googleapis.com \
    networkservices.googleapis.com \
    osconfig.googleapis.com \
    privateca.googleapis.com \
    stackdriver.googleapis.com \
    trafficdirector.googleapis.com
  ```

1. [Increase the default quota](https://console.cloud.google.com/iam-admin/quotas?pageState=(%22expandableQuotasTable%22:(%22f%22:%22%255B%257B_22k_22_3A_22Limit%2520name_22_2C_22t_22_3A10_2C_22v_22_3A_22_5C_22Backend%2520services_5C_22_22_2C_22s_22_3Atrue_2C_22i_22_3A_22displayName_22%257D%255D%22))) for Backend Services (`compute.googleapis.com/backend_services`) to 20.

1. Clone the repo to your local machine or Google Cloud Shell and change
  directory.

  ```shell
  git clone https://gitlab.com/gcp-traffic-director/core-setup
  cd core-setup
  ```

1. Set the environment variables that will be used throughout the demo

  ```shell
  source envvars
  ```

  > **Note 1:** By default, the regions `us-central1` and `asia-southeast1` are used.
  But you can use [any available region](https://cloud.google.com/compute/docs/regions-zones#available).

  > **Note 2:** The `CLUSTER` variable ensures uniqueness in the objects created in
  your project.

  > **Note 3:** You will need to re-apply these variables during teardown of the lab
  or if you do not finish the lab in one sitting.

## Setup options

You have two options to set up Traffic Director.

1. [Manual setup](#option-1-manual-setup): this walks you through each step
using `gcloud`. Because this lab covers many steps, manual setup can take a
while. Most customers would not sit down and walk through each of these steps in
a single sitting. Nonetheless, these steps provide a good sense of the UX when
setting up Traffic Director using `gcloud.`

1. [Automated script setup](#option-2-automated-script-setup): you can
alternatively use shell scripts to set up each step.

## Option 1: Manual setup

This setup is divided into chapters. Each chapter builds on the previous one and
includes demo steps that you can use to understand the concepts.

**Chapter 1: [Global service mesh with external ingress and automatic failover](#global-service-mesh-with-external-ingress-and-automatic-failover)**
  - [Create demo infrastructure](#create-demo-infrastructure)
  - [Create GKE clusters and get them ready for Traffic Director](#create-gke-clusters-and-get-them-ready-for-traffic-director)
    - [Create GKE clusters in each region and create Hub memberships](#create-gke-clusters-and-hub-memberships-for-each-cluster)
    - [Create service accounts for each Kubernetes service](#create-service-accounts-for-each-of-your-kubernetes-services)
    - [Deploy the Envoy sidecar injector](#deploy-the-envoy-sidecar-injector)
  - [Create the Virtual Machine Infrastructure](#create-the-virtual-machine-infrastructure)
  - [Deploy the Microservices Demo application](#deploy-the-microservices-demo-application)
  - [Configure global services and routing in Traffic Director](#configure-global-services-and-routing-in-traffic-director)
    - [Global services and routing for your Kubernetes application workloads](#global-services-and-routing-for-your-kubernetes-application-workloads)
    - [Global services and routing for your Redis workloads](#global-services-and-routing-for-your-redis-workloads)
    - [Enable mesh services to call on external services](#enable-mesh-services-to-call-on-external-services)
  - [Join the mesh (by injecting Envoy sidecar proxies into your Pods)](#join-the-mesh-by-injecting-envoy-sidecar-proxies-into-your-pods)
  - [Set up external HTTP(S) load balancing to ingress traffic from the public internet](#set-up-external-https-load-balancing-to-ingress-traffic-from-the-public-internet)

Demos:
1. [Global service mesh with external ingress](https://docs.google.com/document/d/1bAapIHj6rKK8-97Pn1XMNjyfOg60Qes5SwWvf6jV91w/edit?resourcekey=0-WSzmwZADXdgUIQIOoDGHOg#heading=h.ylmzaonywb97)
1. [Automatic failover](https://docs.google.com/document/d/1bAapIHj6rKK8-97Pn1XMNjyfOg60Qes5SwWvf6jV91w/edit?resourcekey=0-WSzmwZADXdgUIQIOoDGHOg#heading=h.a9rriz7isrji)

**Chapter 2: [Canaries for safe deployments](#canaries-for-safe-deployments)**
  - [Deploy new versions of the `ShippingService` to your clusters](#deploy-new-versions-of-the-shippingservice-to-your-clusters)
  - [Create a global service for your canary](#create-a-global-service-for-your-canary)
  - [Update routing](#update-routing)
  - [Start the canary (or rollback)](#start-the-canary-or-rollback)

Demo: [canaries for safe deployments](https://docs.google.com/document/d/1bAapIHj6rKK8-97Pn1XMNjyfOg60Qes5SwWvf6jV91w/edit?resourcekey=0-WSzmwZADXdgUIQIOoDGHOg#heading=h.h340h5coo1nu)

**Chapter 3: [Zero Trust security](#zero-trust-security)**
  - [...]()

Demo: [Zero Trust security](https://docs.google.com/document/d/1bAapIHj6rKK8-97Pn1XMNjyfOg60Qes5SwWvf6jV91w/edit?resourcekey=0-WSzmwZADXdgUIQIOoDGHOg#heading=h.uv17jxplxdja)

...
  - [Create virtual machines](#create-virtual-machines)
  - [Create client VM](#create-client-vm)
  - [Configure client VM](#configure-client-vm)
  - [Update load balancing](#update-load-balancing)
  - [Apply security](#apply-security)
  - [Configure auto-scaling](#configure-auto-scaling)

## Global service mesh with external ingress and automatic failover

### Create demo infrastructure

The Microservices Demo (featured in this lab) provides a user interface. This
user interface allows you to trigger events and toggle features on/off. Cloud
Pub/Sub topics are used under the hood to signal specific events. Note: use of
Cloud Pub/Sub is not required when using Traffic Director. It is only used to
support this demo.

```shell
for topic in ${TOPICS[@]}
do
  gcloud pubsub topics create $topic
done
```

### Create GKE clusters and get them ready for Traffic Director

#### Create GKE clusters and Hub memberships for each cluster

- Create GKE clusters in both GCP regions, enabling workload identity and
  setting a common identity pool across both clusters. This identity pool will
  be used to enable multi-cluster mutual TLS support.

  ```shell
  gcloud beta container clusters create "${CLUSTER}-${CLUSTER1ZONE}" \
    --release-channel "${CHANNEL}" \
    --zone "${CLUSTER1ZONE}" \
    --num-nodes "${NODES}" \
    --machine-type "${MACHINE}" \
    --scopes=cloud-platform \
    --workload-pool=${IDENTITY_NAMESPACE} \
    --enable-workload-certificates \
    --workload-metadata=GKE_METADATA \
    --subnetwork=default \
    --enable-ip-alias &

  gcloud beta container clusters create "${CLUSTER}-${CLUSTER2ZONE}" \
    --release-channel "${CHANNEL}" \
    --zone "${CLUSTER2ZONE}" \
    --num-nodes "${NODES}" \
    --machine-type "${MACHINE}" \
    --scopes=cloud-platform \
    --workload-pool=${IDENTITY_NAMESPACE} \
    --enable-workload-certificates \
    --workload-metadata=GKE_METADATA \
    --subnetwork=default \
    --enable-ip-alias
  ```

- Export cluster context.

  ```shell
  gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER1ZONE}" \
    --zone "${CLUSTER1ZONE}"
  export CONTEXT_CLUSTER_1=$(kubectl config current-context)

  gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER2ZONE}" \
    --zone "${CLUSTER2ZONE}"
  export CONTEXT_CLUSTER_2=$(kubectl config current-context)
  ```

- Create Hub memberships for each cluster.

  ```shell
  gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER1ZONE} \
    --gke-cluster=${CLUSTER1ZONE}/${CLUSTER}-${CLUSTER1ZONE} \
    --enable-workload-identity

  gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER2ZONE} \
    --gke-cluster=${CLUSTER2ZONE}/${CLUSTER}-${CLUSTER2ZONE} \
    --enable-workload-identity
  ```

#### Create service accounts for each of your Kubernetes services

- Create service accounts that each Kubernetes service will use for
  authentication and authorization purposes. This is required for mutual TLS.

    ```shell
    for account in ${ACCOUNTS[@]}
    do
      kubectl create serviceaccount --namespace "default" $account \
        --cluster ${CONTEXT_CLUSTER_1}
      kubectl create serviceaccount --namespace "default" $account \
        --cluster ${CONTEXT_CLUSTER_2}
    done
    ```

- Allow the Kubernetes service accounts to impersonate the default Compute
  Engine service account by creating an IAM policy binding.
    <!-- TODO: Move the annotations to Kustomize with the rest of the app setup -->
    ```shell
    for account in ${ACCOUNTS[@]}
    do
      # Apply IAM policy binding to enable Workload Identity impersonation
      gcloud iam service-accounts add-iam-policy-binding ${GSA_EMAIL} \
        --role roles/iam.workloadIdentityUser \
        --member "serviceAccount:${IDENTITY_NAMESPACE}[default/$account]"

      # Annotate service accounts in cluster 1
      kubectl annotate --namespace default serviceaccount $account \
        --cluster ${CONTEXT_CLUSTER_1} \
        iam.gke.io/gcp-service-account=${GSA_EMAIL}

      # Annotate service accounts in cluster 2
      kubectl annotate --namespace default serviceaccount $account \
        --cluster ${CONTEXT_CLUSTER_2} \
        iam.gke.io/gcp-service-account=${GSA_EMAIL}
    done
    ```

- Assign required IAM roles to the Google Cloud service account

  ```shell
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_EMAIL}" \
    --role roles/trafficdirector.client

  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_EMAIL}" \
    --role roles/monitoring.metricWriter

  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_EMAIL}" \
    --role roles/cloudtrace.agent

  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_EMAIL}" \
    --role roles/pubsub.admin
  ```

#### Deploy the automatic sidecar proxy injector

Your Kubernetes service Pods need additional infrastructure to be able to
deliver service mesh capabilities. Specifically, they need Envoy sidecar proxies
that handle outbound and inbound requests for your applications. The Envoy
sidecar injector handles installing and bootstrapping these proxies on behalf of
your applications.

1. Download and extract the [Envoy sidecar injector](https://cloud.google.com/traffic-director/docs/set-up-gke-pods-auto)
  ```shell
  wget -O- https://storage.googleapis.com/traffic-director/td-sidecar-injector-xdsv3.tgz | tar xzv
  pushd td-sidecar-injector-xdsv3
  ```

1. Configure the Envoy sidecar injector. This step adds your project number and
   network name to the configmap file.

  ```shell
  sed -i "s/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:.*/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:\ \"${PROJECT_NUM}\"/g" \
   specs/01-configmap.yaml
  sed -i "s/TRAFFICDIRECTOR_NETWORK_NAME:.*/TRAFFICDIRECTOR_NETWORK_NAME:\ \"${NETWORKNAME}\"/g" \
   specs/01-configmap.yaml
  ```

1. Configure TLS for the Envoy sidecar injector. The sidecar injector uses a
  Kubernetes mutating admission webhook to inject proxies when new Pods are
  created. This webhook is an HTTPS endpoint so you need to provide a key and
  certificate for TLS.

  You can create a private key and a self-signed certificate using OpenSSL to
  secure the Envoy sidecar injector:

  ```shell
  CN=istio-sidecar-injector.istio-control.svc

  openssl req \
    -x509 \
    -newkey rsa:4096 \
    -keyout key.pem \
    -out cert.pem \
    -days 365 \
    -nodes \
    -subj "/CN=${CN}"

  cp cert.pem ca-cert.pem
  ```

  Create the namespace under which the Kubernetes secret should be created:

  ```shell
  kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/00-namespaces.yaml
  kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/00-namespaces.yaml
  ```

  Create the secret for the sidecar injector:
  ```shell
  kubectl create --cluster ${CONTEXT_CLUSTER_1} secret generic istio-sidecar-injector -n istio-control \
    --from-file=key.pem \
    --from-file=cert.pem \
    --from-file=ca-cert.pem

  kubectl create --cluster ${CONTEXT_CLUSTER_2} secret generic istio-sidecar-injector -n istio-control \
    --from-file=key.pem \
    --from-file=cert.pem \
    --from-file=ca-cert.pem
  ```

  Modify the `caBundle` of the sidecar injection webhook named
  `istio-sidecar-injector-istio-control` in `specs/02-injector.yaml`:

  ```shell
  CA_BUNDLE=$(cat cert.pem | base64 | tr -d '\n')
  sed -i "s/caBundle:.*/caBundle:\ ${CA_BUNDLE}/g" specs/02-injector.yaml
  ```

1. Install the Envoy sidecar injector to your GKE cluster:

  ```shell
  kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/
  kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/
  ```

1. Return to the original project directory:

  ```shell
  popd
  ```

> **Note:** we just installed the auto-injector to the cluster but we haven't enabled auto-injection just yet. Once we've set up Traffic Director, we'll enable auto-injection by applying the `istio-injection=enabled` label to the `default` namespace. We'll then restart the Pods and Envoy sidecars will be injected. If you enable auto-injection before you configure Traffic Director, your Pods may fail as the Envoy sidecars won't know how to handle traffic.

### Create the Virtual Machine Infrastructure

The `shippingservice` will be deployed on a Managed Instance Group of Compute Engine VMs in each region. The following steps set up the instance templates, provisions Envoy using the using the Auto-installer, and applies a startup script that installs Docker and retrieves the `shippingservice` from the public mircroservices-demo repository.

1. Create the instance templates with [automatic Envoy deployment](https://cloud.google.com/traffic-director/docs/set-up-gce-vms-auto)
    ```shell
    IMAGE_ENTRYPOINT="until wget -O ./entrypoint.sh --no-cache https://storage.googleapis.com/traffic-director-hipster-dev-assets/entrypoint.sh; do sleep 3; done; chmod +x ./entrypoint.sh; exec ./entrypoint.sh \"/shippingservice\""
    gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER1ZONE} \
      --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
      --machine-type=${VIRTUAL_MACHINE} \
      --image-family=debian-10 --image-project=debian-cloud \
      --scopes=https://www.googleapis.com/auth/cloud-platform \
      --region=$CLUSTER1REGION -q \
      --metadata-from-file "startup-script=labs/103-deploy-virtual-machines/startup-script.sh" \
      --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"
    gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER2ZONE} \
      --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
      --machine-type=${VIRTUAL_MACHINE} \
      --image-family=debian-10 --image-project=debian-cloud \
      --scopes=https://www.googleapis.com/auth/cloud-platform \
      --region=$CLUSTER2REGION -q \
      --metadata-from-file "startup-script=labs/103-deploy-virtual-machines/startup-script.sh" \
      --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"
    ```

1. Create the Managed Instance Groups

    Managed Instance Groups are a collection of virtual machine (VM) instances, which allow you to operate them as a stateless, single entity.

    ```shell
    gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER1ZONE} \
      --zone ${CLUSTER1ZONE} --base-instance-name ${CLUSTER}-vm-${CLUSTER1ZONE} \
      --size=1 --template=${CLUSTER}-tpl-${CLUSTER1ZONE}
    gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER2ZONE} \
      --zone ${CLUSTER2ZONE} --base-instance-name ${CLUSTER}-vm-${CLUSTER2ZONE} \
      --size=1 --template=${CLUSTER}-tpl-${CLUSTER2ZONE}
    gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER1ZONE} \
      --named-ports "grpc:8080" --zone ${CLUSTER1ZONE}
    gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER2ZONE} \
      --named-ports "grpc:8080" --zone ${CLUSTER2ZONE}
    ```


### Deploy the Microservices Demo application

1. Deploy the Microservices Demo application (which consists of multiple
    Kubernetes services) to your GKE clusters:

    This step retrieves the microservices-demo from the GoogleCloudPlatform repo.
    It then updates each Kubernetes manifest (using kustomize) to make it ready
    for Traffic Director (by, for example, adding the NEG annotation).

    ```shell
    wget -O labs/102-deploy-application/kubernetes-manifests.yaml \
      https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/master/release/kubernetes-manifests.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -k ./labs/102-deploy-application/
    kubectl apply --cluster ${CONTEXT_CLUSTER_2} -k ./labs/102-deploy-application/
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/102-deploy-application/management-service.yaml
    ```

1. Check to ensure the Pods are deployed:

    ```shell
    kubectl get pods --cluster ${CONTEXT_CLUSTER_1}
    kubectl get pods --cluster ${CONTEXT_CLUSTER_2}
    ```

    You should see an output like the following:
    ```shell
    kubectl get pods --cluster ${CONTEXT_CLUSTER_2}

    NAME                                        READY   STATUS             RESTARTS   AGE
    adservice-9855444-7pnfd                     1/1     Running            0          4m52s
    cartservice-79954f4645-cf5w9                1/1     Running            0          4m52s
    checkoutservice-74c5875d44-pr72h            1/1     Running            0          2m49s
    currencyservice-7f8ccb6f46-wdctz            1/1     Running            0          4m52s
    emailservice-6cf6c8cfbd-vdbzh               1/1     Running            0          4m52s
    frontend-6df5c4c46-2s7dj                    1/1     Running            0          2m29s
    paymentservice-d4b8d7b95-qd8mf              1/1     Running            0          4m51s
    productcatalogservice-6668d98784-2d94f      1/1     Running            0          4m52s
    recommendationservice-85bdbfb7f4-nspzx      1/1     Running            0          4m52s
    redis-cart-5bd446d5b6-jmznt                 1/1     Running            0          4m52s
    shippingservice-786f758dd4-kz9sj            1/1     Running            0          4m52s
    ```

### Set up external HTTP(S) load balancing to ingress traffic from the public internet

At this point, the service mesh has been created. It's time to make it
accessible to the public internet. This step sets up Google Cloud Load
Balancing. Specifically, a global external HTTP(S) load balancer that routes
traffic to:

1. The front-end service in different clusters.
1. The management service (For starting/stopping services via a management
   interface).

#### Create health checks and configure firewall rules

1. Create the health checks. These will be used to health check the backend
   services used by the external HTTP(S) load balancer:

    ```shell
    gcloud compute health-checks create http ${CLUSTER}-external-frontend-hc \
      --use-serving-port \
      --check-interval=1 \
      --healthy-threshold=2 \
      --unhealthy-threshold=3 \
      --timeout=1 \
      --request-path "/_healthz"

    gcloud compute health-checks create http ${CLUSTER}-managementservice-hc \
      --use-serving-port \
      --check-interval=1 \
      --healthy-threshold=2 \
      --unhealthy-threshold=3 \
      --timeout=1 \
      --request-path "/api/health"
    ```

1. Create firewall rules for your health checks. Health checks originate from a
    known range. You need to create firewall rules that permit those health checks
    to reach your instances and/or endpoints:

    ```shell
    gcloud compute firewall-rules create ${CLUSTER}-hc-${NETWORKNAME} \
      --network=${NETWORKNAME} \
      --action=ALLOW \
      --rules=tcp \
      --source-ranges=130.211.0.0/22,35.191.0.0/16

    gcloud compute firewall-rules create ${CLUSTER}-int-${NETWORKNAME} \
      --network=${NETWORKNAME} \
      --action=ALLOW \
      --rules=tcp \
      --source-ranges=10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
    ```
<!-- TODO Restrict ports on the HC and rfc1918 fw rules-->

#### Create global services

1. Create the backend services. Note that you cannot re-use the backend services created for Traffic Director:
    ```shell
    gcloud compute backend-services create ${CLUSTER}-external-frontend-bes \
      --global \
      --protocol HTTP \
      --health-checks ${CLUSTER}-external-frontend-hc

    gcloud compute backend-services create ${CLUSTER}-managementservice-bes \
      --global \
      --protocol HTTP \
      --health-checks ${CLUSTER}-managementservice-hc
    ```

1. Add the front-end application and demo Pods (via NEGs) from each cluster to
   the appropriate backend service.

    ```shell
    gcloud compute backend-services add-backend ${CLUSTER}-external-frontend-bes \
      --global \
      --network-endpoint-group frontend-ext-neg \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5

    gcloud compute backend-services add-backend ${CLUSTER}-external-frontend-bes \
      --global \
      --network-endpoint-group frontend-ext-neg \
      --network-endpoint-group-zone $CLUSTER2ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5

    gcloud compute backend-services add-backend ${CLUSTER}-managementservice-bes \
      --global \
      --network-endpoint-group "managementservice-neg" \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    ```

#### Configure routing and TLS termination for HTTP(S) traffic

To create a managed TLS certificate for the external load balancer we need to
create a fully qualified domain for that certificate. This means that you create
a public DNS record to resolve to the external load balancer's VIP. You can use
any DNS provider and automation that you want. This demo uses Cloud Endpoints
instead of creating a managed DNS zone.

1. Reserve an external IP address:
    ```shell
    gcloud compute addresses create ${CLUSTER}-ext-vip \
      --global \
      --ip-version=IPV4
    ```

1. Create a Google-managed DNS record for the public IP:

  ```shell
  cat <<EOF | tee dns-spec.yaml
  swagger: "2.0"
  info:
    description: "Cloud Endpoints DNS"
    title: "Cloud Endpoints DNS"
    version: "1.0.0"
  paths: {}
  host: "${HOST}"
  x-google-endpoints:
  - name: "${HOST}"
    target: "$(gcloud compute addresses describe ${CLUSTER}-ext-vip --global --format="value(address)")"
  EOF

  gcloud endpoints services deploy dns-spec.yaml
  ```

1. Create a managed SSL Certificate for TLS termination:
  ```shell
  gcloud compute ssl-certificates create ${CLUSTER}-ext-cert \
    --global \
    --domains=$HOST
  ```

1. Create the URL map. This URL map routes all traffic to the front-end
   application except for traffic that needs to go to the demo management
   service:

    ```shell
    gcloud compute url-maps create ${CLUSTER}-ext-map \
      --default-service ${CLUSTER}-external-frontend-bes

    gcloud compute url-maps add-path-matcher ${CLUSTER}-ext-map \
      --default-service ${CLUSTER}-external-frontend-bes \
      --path-matcher-name control-api \
      --path-rules "/api/control=${CLUSTER}-managementservice-bes,/api/logging/*=${CLUSTER}-managementservice-bes"
    ```

1. Create the target proxy:
    ```shell
    gcloud compute target-https-proxies create ${CLUSTER}-ext-proxy \
      --url-map ${CLUSTER}-ext-map \
      --ssl-certificates=${CLUSTER}-ext-cert \
      --quic-override=ENABLE
    ```

1. Create the forwarding rule:
    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-ext-fr \
      --global \
      --address=${CLUSTER}-ext-vip \
      --target-https-proxy=${CLUSTER}-ext-proxy \
      --ports=443
    ```

#### Handle HTTP traffic (by redirecting to HTTPS)

You can configure HTTP->HTTPS redirects in case clients try to access your application using the HTTP protocol.

1. Create a new URL Map:
    ```shell
    envsubst < labs/102-deploy-application/http-to-https-redirect.yaml \
    | gcloud compute url-maps import ${CLUSTER}-http-to-https-redir --global -q
    ```

1. Create a target HTTP proxy to handle HTTP traffic using the redirect URL Map:

    ```shell
    gcloud compute target-http-proxies create ${CLUSTER}-http-to-https-ext-proxy \
      --global \
      --url-map=${CLUSTER}-http-to-https-redir
    ```

1. Create the forwarding rule. Note that the forwarding rule reuses the IP address that you previously used when setting up the external HTTP(S) load balancer:

    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-http-to-https-fr \
      --global \
      --address=${CLUSTER}-ext-vip \
      --target-http-proxy=${CLUSTER}-http-to-https-ext-proxy \
      --ports=80
    ```

After a few minutes, your load balancer should be set up to receive traffic and
forward it to the appropriate k8s Pods. You can verify this by sending a request
to the DNS record that you created. Because we set up a DNS record with Cloud
Endpoints, you should be able to reach your application front-end by visiting
`https://${CLUSTER}.endpoints.${PROJECT_ID}.cloud.goog` in your browser.

Note: if you see an `ERR_SSL_VERSION_OR_CIPHER_MISMATCH` error in your web
browser, the certificate may not be fully provisioned yet. It can take up to 10 minutes for this to complete.

### Configure global services and routing in Traffic Director

<!-- TODO show use of CSDS client to confirm that pods are connected to Traffic Director -->

At this point, you have:

- Two GKE clusters, each running pods for your Kubernetes services.
- Both clusters host the same services in different Google Cloud regions.
- For each pod, we have an application container

Now it's time to set up global services in Traffic Director. These global
services will be automatically load balanced (once routing has been enabled).
Your mesh traffic will also be able to failover seamlessly across clusters in
case of a localized issue.

#### Global services and routing for your Kubernetes application workloads

Traffic Director uses global backend service resources to represent your global
services. You create one backend service per global service and associate the
Pods for each of your Kubernetes services with this backend service. Once this
is done, each backend service is associated with Pods in both of your clusters.

##### Create health checks and firewall rules

1. Create a gRPC health check. Traffic Director uses this health check for
    health-aware load balancing for your services:

    ```shell
    gcloud compute health-checks create grpc ${CLUSTER}-grpc-hc \
      --global \
      --use-serving-port \
      --check-interval=1 \
      --healthy-threshold=2 \
      --unhealthy-threshold=3 \
      --timeout=1
    ```

1. Because you created firewall rules earlier, you don't need to re-create them here.

##### Create global services

1. Create backend services for each service:

    ```shell
    for service in ${SERVICES[@]}
    do
      gcloud compute backend-services create ${CLUSTER}-${service}-bes \
        --global \
        --load-balancing-scheme INTERNAL_SELF_MANAGED \
        --health-checks ${CLUSTER}-grpc-hc \
        --protocol grpc
    done
    ```

1. Check that your backend services were created successfully:

    ```shell
    gcloud compute backend-services list
    ```

    The output should resemble the following:
    ```shell
    NAME                                     BACKENDS                          PROTOCOL
    microsvcdemo-adservice-bes                                                 GRPC
    microsvcdemo-cartservice-bes                                               GRPC
    microsvcdemo-checkoutservice-bes                                           GRPC
    microsvcdemo-currencyservice-bes                                           GRPC
    microsvcdemo-emailservice-bes                                              GRPC
    microsvcdemo-paymentservice-bes                                            GRPC
    microsvcdemo-productcatalogservice-bes                                     GRPC
    microsvcdemo-recommendationservice-bes                                     GRPC
    microsvcdemo-shippingservice-bes                                           GRPC
    ```

1. Add the relevant Pods from each cluster to the appropriate backend service.
   Pods are represented by the Network Endpoint Group (NEG) resource (which was
   created automatically based on the NEG annotation in your Kubernetes service
   manifests).

    ```shell
    for service in ${SERVICES[@]}
    do
      gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
        --global \
        --network-endpoint-group "${service}-neg" \
        --network-endpoint-group-zone $CLUSTER1ZONE \
        --balancing-mode RATE \
        --max-rate-per-endpoint 5
      gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
        --global \
        --network-endpoint-group "${service}-neg" \
        --network-endpoint-group-zone $CLUSTER2ZONE \
        --balancing-mode RATE \
        --max-rate-per-endpoint 5 &
    done
    ```

1. You can `describe` one of the backend services to confirm that your NEGs were
   successfully added. You should see two NEGs in the backend service's
   `backends`:

    ```shell
    gcloud compute backend-services describe ${CLUSTER}-adservice-bes --global
    ```

##### Configure routing

Routing is configured by creating a routing rule map. The routing rule map
consists of three resources -- the URL map, the target proxy and the forwarding
rule.

1. Create a URL map for your gRPC-based services:

    ```shell
    gcloud compute url-maps create ${CLUSTER}-url-map \
      --global \
      --default-service ${CLUSTER}-cartservice-bes
    ```

1. Add URL Map path matchers for each service. Path matchers associate a
   hostname with a backend service. The service mesh infrastructure (Envoy
   sidecar proxies) will match requests to the appropriate backend service based
   on the request hostname and port (for example, `adservice:8080`):

  ```shell
  for service in ${SERVICES[@]}
  do
    gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
      --default-service ${CLUSTER}-${service}-bes \
      --path-matcher-name pathmatch-${service} \
      --new-hosts="${service}:8080"
  done
  ```

1. You can `describe` the URL map to see that hostnames and path matchers were
   successfully added. You should see various `hosts` referencing a
   `pathMatcher` which references a `defaultService`:

    ```shell
    gcloud compute url-maps describe ${CLUSTER}-url-map --global
    ```

1. Create a target gRPC proxy and associate it with the URL map:

    ```shell
    gcloud compute target-grpc-proxies create ${CLUSTER}-grpc-proxy \
      --url-map ${CLUSTER}-url-map
    ```

1. Create the forwarding rule and associate it with the target gRPC proxy. Use
   the `0.0.0.0` catch-all VIP so that your service mesh infrastructure will
   route the request using the request's hostname:

  ```shell
  gcloud compute forwarding-rules create ${CLUSTER}-td-fr \
    --global \
    --load-balancing-scheme INTERNAL_SELF_MANAGED \
    --address=0.0.0.0 \
    --target-grpc-proxy=${CLUSTER}-grpc-proxy \
    --ports 8080 \
    --network $NETWORKNAME
  ```

#### Global services and routing for your Redis workloads

The services created in the previous section were gRPC services. For Redis
services, we need to create a separate service which uses the TCP protocol and
port `6379`. This follows the same general pattern as above.

##### Create health checks and firewall rules

1. Create the TCP health checks for Redis:

    ```shell
    gcloud compute health-checks create tcp ${CLUSTER}-redis-hc \
      --global \
      --port 6379
    ```

##### Create a global service

1. Create a backend service for your Redis workloads (that expect incoming TCP traffic):

    ```shell
    gcloud compute backend-services create ${CLUSTER}-redis-bes \
      --global \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --health-checks ${CLUSTER}-redis-hc \
      --protocol TCP
    ```

1. Add the Redis Pods (via NEGs) from each cluster to the backend service.

    ```shell
    gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
      --network-endpoint-group redis-neg \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode CONNECTION --max-connections-per-endpoint=250

    gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
      --network-endpoint-group redis-neg \
      --network-endpoint-group-zone $CLUSTER2ZONE \
      --balancing-mode CONNECTION --max-connections-per-endpoint=250
    ```

##### Configure routing

1. Create a target TCP Proxy:
    ```shell
    gcloud compute target-tcp-proxies create ${CLUSTER}-redis-proxy \
      --backend-service=${CLUSTER}-redis-bes
    ```

1. Create a forwarding rule for Redis traffic (which will use port `6379`):
    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-redis-fr \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --global \
      --address=0.0.0.0 \
      --target-tcp-proxy=${CLUSTER}-redis-proxy \
      --ports 6379 \
      --network $NETWORKNAME
    ```

#### Global Services and Routing for Compute Engine VMs

##### Create global services

1. Create the backend for `shippingservice`.
    ```shell
    gcloud compute backend-services create ${CLUSTER}-shippingservice-vm-bes --global \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --connection-draining-timeout=30s --port-name=grpc \
      --health-checks ${CLUSTER}-grpc-hc --protocol grpc
    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-vm-bes \
      --instance-group ${CLUSTER}-mig-${CLUSTER1ZONE} \
      --instance-group-zone ${CLUSTER1ZONE} \
      --global
    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-vm-bes \
      --instance-group ${CLUSTER}-mig-${CLUSTER2ZONE} \
      --instance-group-zone ${CLUSTER2ZONE} \
      --global
    ```

##### Configure routing

1. Create a forwarding rule for Compute Engine VMs.

    Because you can't use an anycast VIP with Compute Engine currently, we need
    to create a unicast VIP instead. The /etc/hosts file on the VM ensures
    services resolve to 10.254.254.101 as well.
    See [http://b/157713293](http://b/157713293)

    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-td-fr-vip \
      --global \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --address=10.254.254.101 \
      --target-grpc-proxy=${CLUSTER}-grpc-proxy \
      --ports 8080 \
      --network $NETWORKNAME
    ```

1. Configure the path matcher to use the VM-based backend-service.
    ```shell
    gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
      --default-service ${CLUSTER}-shippingservice-vm-bes \
      --path-matcher-name pathmatch-shippingservice --new-hosts="shippingservice:8080"
    ```

#### Enable mesh services to call on external services

By default, if your applications attempt to access an external service (one that
is not explicitly configured in Traffic Director), the request will be dropped.
This is because your service mesh infrastructure doesn't know how to handle the
request. You can change this behavior so that calls to external services just
"pass through" the service mesh infrastructure and don't get dropped.

This allows the containers to access the Google APIs required to function in
this demo. Note that this backend service will not have any backends or health
check associated with it.

##### Create a global service to represent external services

1. Create a backend service with the `ORIGINAL_DESTINATION` locality load
  balancing policy and the `TCP` protocol.

  ```shell
  gcloud compute backend-services import ${CLUSTER}-external-services --global -q <<EOF
  name: ${CLUSTER}-external-services
  loadBalancingScheme: INTERNAL_SELF_MANAGED
  protocol: TCP
  localityLbPolicy: ORIGINAL_DESTINATION
  EOF
  ```

##### Configure routing to enable default passthrough

1. Create a target TCP Proxy to capture external traffic to the mesh, such as
   Google APIs:

    ```shell
    gcloud compute target-tcp-proxies create ${CLUSTER}-tcp-proxy \
      --backend-service=${CLUSTER}-external-services
    ```

1. Enable routing to external services by creating a forwarding rule. Use the
   `0.0.0.0` catch-all VIP for the forwarding rule and TCP port 443 so that you
   can call on any external HTTPS service, regardless of its IP address or
   protocol. This is used to process traffic to Google Cloud APIs.

    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-td-fr-443 \
      --global \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --address=0.0.0.0 \
      --target-tcp-proxy=${CLUSTER}-tcp-proxy \
      --ports 443 \
      --network $NETWORKNAME
    ```

#### Join the mesh (by injecting Envoy sidecar proxies into your Pods)

Now that Traffic Director has been configured, enable auto-injection and restart
your Pods. This will make them part of the mesh.

1. Apply the `istio-injectio=enabled` label to the `default` namespace. This
    activates the mutation webhook to inject sidecar proxies into your
    deployments:

    ```shell
    kubectl label --cluster ${CONTEXT_CLUSTER_1} namespace default \
    istio-injection=enabled
    kubectl label --cluster ${CONTEXT_CLUSTER_2} namespace default \
    istio-injection=enabled
    ```

    Note: Traffic Director re-uses the Istio sidecar auto-injector. Traffic Director
    is not Istio. To avoid potential issues, do not use both Istio and Traffic
    Director on the same GKE cluster.

1. Now perform a rolling update of the Deployments, which injects sidecar
    proxies into the Deployment Pods:

    ```shell
    kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_1}
    kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_2}
    ```

    If you check your Pods now, you should see all Pods with the `Running` status
    and `2/2`, indicating that both your workload and Envoy sidecars are running:

    ```shell
    kubectl get pods --cluster ${CONTEXT_CLUSTER_2}

    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-557dbf58b-7zssb                2/2     Running   0          6m31s
    cartservice-57764cfbb9-676qc             2/2     Running   0          6m31s
    checkoutservice-6945959947-s8fq5         2/2     Running   0          6m31s
    currencyservice-7bc5645675-j8p5c         2/2     Running   0          6m30s
    emailservice-b4c4fdb95-h7g8t             2/2     Running   0          6m30s
    frontend-6594f7fc4d-69vdx                2/2     Running   0          6m29s
    paymentservice-64dcf9c9b4-hkk67          2/2     Running   0          6m28s
    productcatalogservice-5b6555d677-7kgcp   2/2     Running   0          6m28s
    recommendationservice-796bb95f89-m7bw6   2/2     Running   0          6m28s
    redis-cart-6889b8fc5b-fxg7s              1/1     Running   0          6m27s
    shippingservice-5cd455584f-h48g9         2/2     Running   0          6m27s
    ```

### Verify Connectivity


Your application is now ready to use. Access the application by generating the hyperlink below.

  ```shell
  printf "https://$(gcloud endpoints services list --format="value(serviceName)" | grep "$CLUSTER")\n"
  ```
> **Note:** If you get an unsupported protocol error, please continue to wait until the managed certificate is fully provisioned.

## Canaries for safe deployments

This section creates the enhanced `ShippingService` which is a replacement for
the existing `ShippingService`. It will be deployed safely using a canary. In
this approach, 50% of traffic will be served using the new version while 50%
will be served using the existing version.

### Deploy new versions of the `ShippingService` to your clusters

- Deploy `ShippingService-enhanced` to the GKE clusters in both regions:

  ```shell
  kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
  kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
  ```

### Create a global service for your canary

Create a new backend service to represent the enhanced `ShippingService`. The
backends for this service will reference the Pods belonging to the version that
you just deployed to your clusters.

1. Create a new backend service:
    ```shell
    gcloud compute backend-services create ${CLUSTER}-shippingservice-enhanced-bes \
      --global \
      --load-balancing-scheme INTERNAL_SELF_MANAGED \
      --health-checks ${CLUSTER}-grpc-hc \
      --protocol grpc
    ```

1. Add the enhanced shipping service Pods (via NEGs) from each cluster to this
  backend service.

    ```shell
    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
      --global \
      --network-endpoint-group "shippingservice-enhanced-neg" \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5

    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
      --global \
      --network-endpoint-group "shippingservice-enhanced-neg" \
      --network-endpoint-group-zone $CLUSTER2ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    ```

### Update routing

In this section, we get the URL map and create a version of the URL map which
sends 50% of traffic to the enhanced `ShippingService`. We keep an original and
an updated copy in case we want to rollback.

- Export the URL Map to your local filesystem:
  ```shell
  gcloud compute url-maps export ${CLUSTER}-url-map \
    --global \
    --destination ${CLUSTER}-url-map-original.yaml
  ```

- Update the canary URL map to split traffic across the original `ShippingService` and enhanced `ShippingService` backend services.
    ```shell
    PATH_MATCH_RULES="  routeRules:
      - priority: 0
        matchRules:
        - prefixMatch: ''
        routeAction:
          weightedBackendServices:
          - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
            weight: 50
          - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-enhanced-bes
            weight: 50"
    PATH_MATCH_RULES=$(echo "$PATH_MATCH_RULES" | sed "s/\\//\\\\\\//g" | tr '\n' '\r')

    URL_MAP=$(cat ./${CLUSTER}-url-map-original.yaml | tr '\n' '\r' | sed "s/  name: pathmatch-shippingservice/  name: pathmatch-shippingservice\r${PATH_MATCH_RULES}/" | tr '\r' '\n')
    echo "$URL_MAP" > ./${CLUSTER}-url-map-canary.yaml
    ```

- You can now compare the two URL maps. Notice that the path matcher for the `ShippingService` in the original URL map routes all traffic to `${CLUSTER}-shippingservice-bes`.

    ```shell
    cat ${CLUSTER}-url-map-original.yaml

    ...
    - defaultService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
    name: pathmatch-shippingservice
    ...
    ```

- The path matcher for the `ShippingService` in the canary URL map does a 50/50 traffic split across `${CLUSTER}-shippingservice-bes` and `${CLUSTER}-shippingservice-enhanced-bes`.

    ```shell
    cat ${CLUSTER}-url-map-canary.yaml

    ...
    - defaultService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
    name: pathmatch-shippingservice
    routeRules:
    - priority: 0
      matchRules:
      - prefixMatch: ''
      routeAction:
        weightedBackendServices:
        - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
          weight: 50
        - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-enhanced-bes
          weight: 50
    ...
    ```

### Start the canary (or rollback)

- Finally, import the updated URL Map to start the canary deployment:

    ```shell
    gcloud compute url-maps import ${CLUSTER}-url-map \
      --global \
      --source ${CLUSTER}-url-map-canary.yaml \
      -q
    ```

- If you want to rollback, just import the original URL Map:

    ```shell
    gcloud compute url-maps import ${CLUSTER}-url-map \
      --global \
      --source ${CLUSTER}-url-map-original.yaml \
      -q
    ```

## Zero Trust security

...

--



### Create client VM

To access the internal load balancer, you'll need to provision a VM inside the same network used by the cluster. This also provides an opportunity to access the external load balancer from a different physical location to your own (e.g. if you are in the US, you may wish to provision the VM below in asia-southeast1-b to see external requests routed through a different cluster).

1. Create a virtual machine
    ```shell
    gcloud compute instances create ${CLUSTER}-vm-client \
      --image-project debian-cloud --image-family debian-10 \
      --zone $CLUSTER1ZONE \
      --metadata-from-file "startup-script=labs/104-deploy-client-vm/startup-script.sh"
    ```

### Configure client VM

1. Wait for the startup script to run. This can take up to 10 minutes to complete. To monitor progress, connect to the VM instance:
    ```shell
    gcloud compute ssh ${CLUSTER}-vm-client
    ```

    And then run the following command:
    ```shell
    sudo journalctl -o cat -f _SYSTEMD_UNIT=google-startup-scripts.service
    ```

    This command shows the output from the startup script. When the script has finished, you see the following:

    ```shell
    INFO startup-script: Chrome remote desktop installation completed
    INFO startup-script: Return code 0.
    INFO Finished running startup scripts.
    ```

1. Once complete, you still need to configure the Remote Desktop Service for your user as described [here](https://cloud.google.com/solutions/chrome-desktop-remote-on-compute-engine#configuring_and_starting_the_chrome_remote_desktop_service).
    > Note that the package is already installed so this step can be skipped.

### Update load balancing

1. Deploy internal frontend, mobile frontend, and a new version of the shipping service.
    ```shell
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-internal.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/frontend-mobile.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    ```

1. Add new backend services
    ```shell
    gcloud compute backend-services create ${CLUSTER}-frontend-internal-bes \
      --health-checks ${CLUSTER}-external-frontend-hc --protocol http \
      --load-balancing-scheme INTERNAL_MANAGED --region $CLUSTER1REGION
    gcloud compute backend-services create ${CLUSTER}-frontend-mobile-bes \
      --global --health-checks ${CLUSTER}-external-frontend-hc --protocol http \
      --load-balancing-scheme EXTERNAL
    gcloud compute backend-services create ${CLUSTER}-shippingservice-enhanced-bes \
      --global --health-checks ${CLUSTER}-grpc-hc --protocol grpc \
      --load-balancing-scheme INTERNAL_SELF_MANAGED
    ```

1. Add backends to new backend services
    ```shell
    gcloud compute backend-services add-backend ${CLUSTER}-frontend-internal-bes \
      --region $CLUSTER1REGION \
      --network-endpoint-group "frontend-internal-neg" \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    gcloud compute backend-services add-backend ${CLUSTER}-frontend-mobile-bes \
      --global \
      --network-endpoint-group "frontend-mobile-neg" \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    gcloud compute backend-services add-backend ${CLUSTER}-frontend-mobile-bes \
      --global \
      --network-endpoint-group "frontend-mobile-neg" \
      --network-endpoint-group-zone $CLUSTER2ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
      --global \
      --network-endpoint-group "shippingservice-enhanced-neg" \
      --network-endpoint-group-zone $CLUSTER1ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    gcloud compute backend-services add-backend ${CLUSTER}-shippingservice-enhanced-bes \
      --global \
      --network-endpoint-group "shippingservice-enhanced-neg" \
      --network-endpoint-group-zone $CLUSTER2ZONE \
      --balancing-mode RATE \
      --max-rate-per-endpoint 5
    ```

1. Create a URL map for internal requests
    ```shell
    gcloud compute url-maps create ${CLUSTER}-int-map \
      --default-service=${CLUSTER}-frontend-internal-bes \
      --region=$CLUSTER1REGION
    ```

1. Create the target proxy for the internal service
    ```shell
    gcloud compute target-http-proxies create ${CLUSTER}-int-proxy \
      --url-map=${CLUSTER}-int-map \
      --url-map-region=$CLUSTER1REGION \
      --region=$CLUSTER1REGION
    ```

1. Create a subnet for the internal load balancer
    ```shell
    gcloud compute networks subnets create proxy-subnet \
      --purpose=INTERNAL_HTTPS_LOAD_BALANCER \
      --role=ACTIVE \
      --network=$NETWORKNAME \
      --range=10.100.0.0/24 \
      --region=$CLUSTER1REGION
    ```

1. Create a forwarding rule for the internal service
    ```shell
    gcloud compute forwarding-rules create ${CLUSTER}-int-fr \
      --load-balancing-scheme INTERNAL_MANAGED \
      --region=$CLUSTER1REGION \
      --target-http-proxy=${CLUSTER}-int-proxy \
      --target-http-proxy-region=$CLUSTER1REGION \
      --ports=80
    ```

1. Update the external URL map to include the new mobile frontend service
    <!-- TODO Make these files and use envsubst -->
    ```shell
    gcloud compute url-maps import ${CLUSTER}-ext-map \
      --global -q <<EOF
    defaultService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-external-frontend-bes
    hostRules:
    - hosts:
      - '*'
      pathMatcher: control-api
    kind: compute#urlMap
    name: ${CLUSTER}-ext-map
    pathMatchers:
    - defaultService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-external-frontend-bes
      name: control-api
      routeRules:
        - matchRules:
            - prefixMatch: /api
          priority: 0
          service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-managementservice-bes
        - matchRules:
            - headerMatches:
                - headerName: user-agent
                  prefixMatch: Mozilla/5.0 (Linux; Android
          priority: 1
          service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-frontend-mobile-bes
        - matchRules:
            - headerMatches:
                - headerName: user-agent
                  prefixMatch: Mozilla/5.0 (iPhone
          priority: 2
          service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-frontend-mobile-bes
        - matchRules:
            - prefixMatch: /
          priority: 3
          service: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-external-frontend-bes
    selfLink: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/urlMaps/${CLUSTER}-ext-map
    EOF
    ```

1. Update the internal URL map to include the new shipping service
    ```shell
    gcloud compute url-maps export ${CLUSTER}-url-map \
      --global --destination url-map.yaml

    PATH_MATCH_RULES="  routeRules:
      - priority: 0
        matchRules:
        - prefixMatch: ''
        routeAction:
          weightedBackendServices:
          - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-bes
            weight: 50
          - backendService: https://www.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-shippingservice-enhanced-bes
            weight: 50"
    PATH_MATCH_RULES=$(echo "$PATH_MATCH_RULES" | sed "s/\\//\\\\\\//g" | tr '\n' '\r')

    URL_MAP=$(cat ./url-map.yaml | tr '\n' '\r' | sed "s/  name: pathmatch-shippingservice/  name: pathmatch-shippingservice\r${PATH_MATCH_RULES}/" | tr '\r' '\n')
    echo "$URL_MAP" > ./url-map.yaml

    gcloud compute url-maps import ${CLUSTER}-url-map \
      --global --source url-map.yaml --quiet

    rm ./url-map.yaml
    ```

### Apply security

1. Switch to using the GKE-based checkout service
    ```shell
    gcloud compute url-maps remove-path-matcher ${CLUSTER}-url-map \
      --path-matcher-name pathmatch-checkoutservice
    gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
      --default-service ${CLUSTER}-checkoutservice-bes \
      --path-matcher-name pathmatch-checkoutservice --new-hosts="checkoutservice:8080"
    ```
#### Set up private certificate authority

1. Create a root CA:
    ```shell
    gcloud beta privateca roots create ${CLUSTER}-root \
      --location $CLUSTER1REGION \
      --subject "CN=${CLUSTER}-root, O=Traffic Director Demo" \
      --tier "enterprise"
    ```
1. Create the subordinate CAs in each region:
    ```shell
    gcloud beta privateca subordinates create ${CLUSTER}-${CLUSTER1REGION}-sub \
      --issuer ${CLUSTER}-root \
      --issuer-location ${CLUSTER1REGION} \
      --subject "CN=${CLUSTER}-${CLUSTER1REGION}-sub, O=Traffic Director Demo" \
      --location ${CLUSTER1REGION} \
      --tier devops

    gcloud beta privateca subordinates create ${CLUSTER}-${CLUSTER2REGION}-sub \
      --issuer ${CLUSTER}-root \
      --issuer-location ${CLUSTER1REGION} \
      --subject "CN=${CLUSTER}-${CLUSTER2EGION}-sub, O=Traffic Director Demo" \
      --location ${CLUSTER2REGION} \
      --tier devops
    ```

1. Grant the IAM `privateca.auditor` role for the root CA and the
  `privateca.certificateManager` for the subordinate CAs to allow access from
  the GKE service account:

  ```shell
  gcloud beta privateca roots add-iam-policy-binding ${CLUSTER}-root \
    --location=${ROOT_CA_REGION} \
    --role roles/privateca.auditor \
    --member="serviceAccount:${GKE_SA}"

  gcloud beta privateca subordinates add-iam-policy-binding ${CLUSTER}-${CLUSTER1REGION}-sub \
    --location=${CLUSTER1REGION} \
    --role roles/privateca.certificateManager \
    --member="serviceAccount:${GKE_SA}"

  gcloud beta privateca subordinates add-iam-policy-binding ${CLUSTER}-${CLUSTER2REGION}-sub \
    --location=${CLUSTER2REGION} \
    --role roles/privateca.certificateManager \
    --member="serviceAccount:${GKE_SA}"
  ```

1. Instruct the clusters how to issue certificates
  ```shell
  REGION=${CLUSTER1REGION} envsubst < labs/106-deploy-security/WorkloadCertificateConfig-TrustConfig.yaml | \
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f -

  REGION=${CLUSTER2REGION} envsubst < labs/106-deploy-security/WorkloadCertificateConfig-TrustConfig.yaml | \
    kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f -
  ```

1. Configure IAM to allow the default service account to access the Traffic Director API.
  ```shell
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${GSA_EMAIL} \
    --role roles/trafficdirector.client
  ```

1. Annotate PodSpec template for all Deployments that will need to participate
  in mTLS encryption; this is done by enabling pods to mount managed
  certificates provided by their SPIFFE IDs based on their Kubernetes service
  account names:

  ```shell
  for service in ${MTLS_SERVICES[@]}
  do
    kubectl patch --namespace default deployment $service \
      --cluster ${CONTEXT_CLUSTER_1} \
      --patch '{"spec":{"template":{"metadata":{"annotations":{"cloud.google.com/enableManagedCerts": "true"}}}}}'
    kubectl patch --namespace default deployment $service \
      --cluster ${CONTEXT_CLUSTER_2} \
      --patch '{"spec":{"template":{"metadata":{"annotations":{"cloud.google.com/enableManagedCerts": "true"}}}}}'
  done
  ```

  Once Deployments are patched, perform a rolling update to apply the new
  configuration:

  ```shell
  kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_1}
  kubectl rollout restart deployment --cluster ${CONTEXT_CLUSTER_2}
  ```

1. Create client mTLS policy
  ```shell
  gcloud beta network-security client-tls-policies import ${CLUSTER}-client-mtls-policy \
    --location=global <<EOF
  name: "${CLUSTER}-client-mtls-policy"
  clientCertificate:
    certificateProviderInstance:
      pluginInstance: google_cloud_private_spiffe
  serverValidationCa:
  - certificateProviderInstance:
      pluginInstance: google_cloud_private_spiffe
  EOF
  ```

1. Attach client TLS policy to backend services

  ```shell
  for service in ${SERVICES[@]}
  do
    gcloud compute backend-services import ${CLUSTER}-${service}-bes --global \
      --quiet <<EOF
    securitySettings:
      clientTlsPolicy: projects/${PROJECT_ID}/locations/global/clientTlsPolicies/${CLUSTER}-client-mtls-policy
      subjectAltNames:
        - "spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/${service}"
    EOF

    echo "Patched ${service} backend service"
  done
  ```

1. Create server TLS policy
    ```shell
    gcloud beta network-security server-tls-policies import ${CLUSTER}-server-mtls-policy \
      --location=global <<EOF
    name: "${CLUSTER}-server-mtls-policy"
    serverCertificate:
      certificateProviderInstance:
        pluginInstance: google_cloud_private_spiffe
    mtlsPolicy:
      clientValidationCa:
      - certificateProviderInstance:
          pluginInstance: google_cloud_private_spiffe
    EOF
    ```

1. Create an endpoint policy
    ```shell
    gcloud alpha network-services endpoint-policies import ${CLUSTER}-endpoint-policy \
      --location=global <<EOF
    endpointMatcher:
      metadataLabelMatcher:
        metadataLabelMatchCriteria: MATCH_ALL
        metadataLabels:
          - labelName: app
            labelValue: hipster
    name: "endpoint-policy"
    serverTlsPolicy: projects/${PROJECT_ID}/locations/global/serverTlsPolicies/${CLUSTER}-server-mtls-policy
    type: SIDECAR_PROXY
    EOF
    ```

1. Create authorization policy
    ```shell
    gcloud alpha network-security authorization-policies import ${CLUSTER}-authz-policy \
      --location=global <<EOF
    action: ALLOW
    name: ${CLUSTER}-authz-policy
    rules:
    - sources:
      - principals:
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/adservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/cartservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/checkoutservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/currencyservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/emailservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/frontend
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/paymentservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/productcatalogservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/recommendationservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/shippingservice
        - spiffe://${IDENTITY_NAMESPACE}/ns/default/sa/shippingservice-enhanced
      destinations:
      - hosts:
        - adservice:8080
        - cartservice:8080
        - checkoutservice:8080
        - currencyservice:8080
        - emailservice:8080
        - paymentservice:8080
        - productcatalogservice:8080
        - recommendationservice:8080
        - shippingservice:8080
        ports:
        - 3550
        - 5050
        - 7000
        - 7070
        - 8080
        - 9555
        - 50051
    EOF
    ```

1. Update endpoint policy with the authorization policy
  ```shell
  gcloud alpha network-services endpoint-policies import ${CLUSTER}-endpoint-policy \
    --location=global <<EOF
  endpointMatcher:
    metadataLabelMatcher:
      metadataLabelMatchCriteria: MATCH_ALL
      metadataLabels:
        - labelName: app
          labelValue: hipster
  name: "endpoint-policy"
  serverTlsPolicy: projects/${PROJECT_ID}/locations/global/serverTlsPolicies/${CLUSTER}-server-mtls-policy
  type: SIDECAR_PROXY
  authorizationPolicy: projects/${PROJECT_ID}/locations/global/authorizationPolicies/${CLUSTER}-authz-policy
  EOF
  ```

### Configure auto-scaling

1. Enable auto-scale on the GKE clusters
    ```shell
    gcloud container clusters update ${CLUSTER}-${CLUSTER1ZONE} --enable-autoscaling \
      --min-nodes 3 --max-nodes 10 --zone ${CLUSTER1ZONE} \
      --node-pool default-pool
    gcloud container clusters update ${CLUSTER}-${CLUSTER2ZONE} --enable-autoscaling \
      --min-nodes 3 --max-nodes 10 --zone ${CLUSTER2ZONE} \
      --node-pool default-pool
    ```

1. Enable autoscale on the virtual machines
    ```shell
    gcloud compute instance-groups managed set-autoscaling ${CLUSTER}-mig-${CLUSTER1ZONE} \
      --max-num-replicas 5 --zone ${CLUSTER1ZONE}
    gcloud compute instance-groups managed set-autoscaling ${CLUSTER}-mig-${CLUSTER2ZONE} \
      --max-num-replicas 5 --zone ${CLUSTER2ZONE}
    ```

1. Apply horizontal pod autoscale configuration on each pod
    ```shell
    kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/107-deploy-autoscaling/autoscale.yaml
    kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f ./labs/107-deploy-autoscaling/autoscale.yaml
    ```

1. Disable advanced logging on cluster pods
    > By default, the microservices-demo logs every request, which is not recommended for production (or a scenario like this one where we generate non-trival load).
    ```shell
    sed -i "s/        # - name: DISABLE_TRACING/        - name: DISABLE_TRACING/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        # - name: DISABLE_PROFILER/        - name: DISABLE_PROFILER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        # - name: DISABLE_DEBUGGER/        - name: DISABLE_DEBUGGER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        # - name: DISABLE_STATS/        - name: DISABLE_STATS/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        #   value: \"1\"/          value: \"1\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/          value: \"10\"/          value: \"50\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/            value: \"true\"/            value: \"false\"/g" ./labs/102-deploy-application/deployments-patch.yaml

    sed -i "s/        # - name: DISABLE_TRACING/        - name: DISABLE_TRACING/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/        # - name: DISABLE_PROFILER/        - name: DISABLE_PROFILER/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/        # - name: DISABLE_STATS/        - name: DISABLE_STATS/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/          value: \"true\"/          value: \"false\"/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    ```

1. Apply updated manifests to the clusters
    ```shell
    kubectl apply --cluster $CONTEXT_CLUSTER_1 -k ./labs/102-deploy-application/
    kubectl apply --cluster $CONTEXT_CLUSTER_2 -k ./labs/102-deploy-application/
    kubectl apply --cluster $CONTEXT_CLUSTER_1 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    kubectl apply --cluster $CONTEXT_CLUSTER_2 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    ```

1. Revert changes to configuration
    ```shell
    sed -i "s/        - name: DISABLE_TRACING/        # - name: DISABLE_TRACING/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        - name: DISABLE_PROFILER/        # - name: DISABLE_PROFILER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        - name: DISABLE_DEBUGGER/        # - name: DISABLE_DEBUGGER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/        - name: DISABLE_STATS/        # - name: DISABLE_STATS/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/          value: \"1\"/        #   value: \"1\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/          value: \"50\"/          value: \"10\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
    sed -i "s/            value: \"false\"/            value: \"true\"/g" ./labs/102-deploy-application/deployments-patch.yaml

    sed -i "s/        - name: DISABLE_TRACING/        # - name: DISABLE_TRACING/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/        - name: DISABLE_PROFILER/        # - name: DISABLE_PROFILER/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/        - name: DISABLE_STATS        # - name: DISABLE_STATS/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    sed -i "s/          value: \"false\"/          value: \"true\"/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
    ```

1. Scale up load generator pods to prompt autoscaling activity
    ```shell
    kubectl scale --cluster $CONTEXT_CLUSTER_1 deployment/loadgenerator --replicas 5
    kubectl scale --cluster $CONTEXT_CLUSTER_2 deployment/loadgenerator --replicas 5
    ```

## Option 2: Automated script setup

These scripts will build the service out in ~10 minutes automatically.

  ```shell
  source labs/101-deploy-cluster/cluster.sh
  bash labs/102-deploy-application/application.sh
  bash labs/103-deploy-virtual-machines/virtual-machine.sh
  bash labs/104-deploy-client-vm/client-vm.sh
  bash labs/105-deploy-load-balancing/load-balancing.sh
  bash labs/106-deploy-security/security.sh
  bash labs/107-deploy-autoscaling/autoscaling.sh
  ```

## Cleaning up

### Do it manually

Before running these commands, ensure that your environment variables are set:

   ```shell
   source envvars
   ```

#### Delete Traffic Director and Cloud Load Balancing resources

1. Delete forwarding rules:
    ```shell
    for item in $(gcloud compute forwarding-rules list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute forwarding-rules delete \
        --global \
        -q \
        $item
    done
    ```

1. Delete target proxies:
    ```shell
    for item in $(gcloud compute target-http-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute target-http-proxies delete \
        -q \
        $item
    done
    for item in $(gcloud compute target-grpc-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute target-grpc-proxies delete \
        -q \
        $item
    done
    for item in $(gcloud compute target-https-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute target-https-proxies delete \
        -q \
        $item
    done
    for item in $(gcloud compute target-tcp-proxies list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute target-tcp-proxies delete \
        -q \
        $item
    done
    ```

1. Delete URL maps:
    ```shell
    for item in $(gcloud compute url-maps list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute url-maps delete \
        --global \
        -q \
        $item
    done
    ```

1. Delete backend services:
    ```shell
    for item in $(gcloud compute backend-services list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute backend-services delete \
        --global \
        -q \
        $item
    done
    ```

1. Delete health checks:
    ```shell
    for item in $(gcloud compute health-checks list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute health-checks delete \
        --global \
        -q \
        $item
    done
    ```

1. Delete SSL certs:
    ```shell
    for item in $(gcloud compute ssl-certificates list --format="value(name)" --filter="name~'${CLUSTER}-'")
    do
      gcloud compute ssl-certificates delete \
        --global \
        -q \
        $item
    done
    ```

#### Delete Cloud Endpoints DNS record

1. Delete Cloud Endpoints service:
    ```shell
    for item in $(gcloud endpoints services list --format="value(NAME)" --filter="NAME~'${CLUSTER}'")
    do
      gcloud endpoints services delete $item \
        -q
    done
    ```

#### Delete GKE clusters

1. Delete GKE clusters:
    ```shell
    gcloud container clusters delete ${CLUSTER}-${CLUSTER1ZONE} \
      --region ${CLUSTER1ZONE} \
      -q

    gcloud container clusters delete ${CLUSTER}-${CLUSTER2ZONE} \
      --region ${CLUSTER2ZONE} \
      -q
    ```

### Use the automated scripts

Just use the scripts. It's easier!

  ```shell
  bash labs/107-deploy-autoscaling/delete-autoscaling.sh
  bash labs/106-deploy-security/delete-security.sh
  bash labs/105-deploy-load-balancing/delete-load-balancing.sh
  bash labs/104-deploy-client-vm/delete-client-vm.sh
  bash labs/103-deploy-virtual-machines/delete-virtual-machine.sh
  bash labs/102-deploy-application/delete-application.sh
  bash labs/101-deploy-cluster/delete-cluster.sh
  ```

N.B. The scripts do not remove service accounts (as these do not incur a charge and can be reused for future deployments).

## Demo implementation

When assembling this demo, there were two primary objectives:

1. Avoid forking the microservices-demo code base: staying as close to the main branch as possible ensures this demo can take advantage of the continuing improvements and fixes in the code base.

1. Avoid additional traffic over the service mesh: any control plane for the demo should ideally operate outside the HTTP/gRPC channels managed by Traffic Director to avoid generating traffic patterns that do not otherwise exist in the app.

As a result, the demo architecture has been implemented as follows:

* Each service has a new entrypoint (see `deployments-patch.yaml`). This entrypoint is an inline script that fetches a proxy (see `src/td-demo-client`) and runs it in front of the core service. The core service ends up running on `$PORT+1` and is not externally accessible. This proxy does a few things:
    * Health checks: it exposes a health check to Traffic Director via a separate port. This health check is independent of the core service and can be controlled through the demo UI (although if the underlying service is unhealthy, this state will be passed through). By using a separate port, the security policy applied to the mesh does not interfere with Traffic Director health checks. N.B. this health check is independent to the k8s pod health check, which does access the actual service (as returning an unhealthy state to k8s, as we do to Traffic Director, results in an unwanted pod restart).
    * Control: on start, the pod queries the management service for the state to report to Traffic Director (i.e. healhty or unhealthy). This is done by publishing a message on a well-known Pub/Sub topic and listening for a response on a pod-specific subscription. It continues to listen for updates on this subscription throughout its lifetime (and, when shutdown cleanly, should delete the subscription).
    * Logging: each message intercepted by the proxy is logged to a well known Pub/Sub topic (and processed by the management service). This message includes the pod ID, the service name, the cluster zone, and the certificate used (if any).

* A management service runs in `us-central1-c`: this processes incoming Pub/Sub messages and exposes an API that can be queried by the frontend to control the health of services and fetch logging information. Logging information is stored temporarily in an in-memory cache.

* Minor updates to the microservices-demo code base: the only intrusive code change is to replace the existing request ID featured on the home page with the one used by Stackdriver. This ID is passed through the service mesh and is attached to each logging message. By including it on the home page, the new client-side script file (`script.js`) can query the management service API to populate the control panel. All other functionality is appended where ever possible (e.g. to the end of CSS files) rather than in specific code locations.

---
This is not an official Google project (yet).
