#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-microsvcdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
VIRTUAL_MACHINE="${VIRTUAL_MACHINE:-e2-standard-2}"
IMAGE_LOCATION=gcr.io/google-samples/microservices-demo/checkoutservice:v0.2.2
IMAGE_ENTRYPOINT="until wget -O ./entrypoint.sh --no-cache https://storage.googleapis.com/traffic-director-hipster-dev-assets/entrypoint.sh; do sleep 3; done; chmod +x ./entrypoint.sh; exec ./entrypoint.sh \"/checkoutservice\""
yellow='\033[1;33m'
nc='\033[0m' # No Color

#Uses the new Envoy Injector:
#https://cloud.google.com/traffic-director/docs/set-up-gce-vms-auto
echo -e "Creating ${yellow}Instance Templates${nc}."
gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER1ZONE} \
  --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
  --machine-type=${VIRTUAL_MACHINE} \
  --image-family=debian-10 --image-project=debian-cloud \
  --scopes=https://www.googleapis.com/auth/cloud-platform \
  --region=`echo $CLUSTER1ZONE | cut -f1-2 -d '-'` -q \
  --metadata-from-file "startup-script=$(dirname "$0")/startup-script.sh" \
  --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"
gcloud compute instance-templates create ${CLUSTER}-tpl-${CLUSTER2ZONE} \
  --service-proxy enabled,tracing=ON,access-log=/var/log/envoy/access.log \
  --machine-type=${VIRTUAL_MACHINE} \
  --image-family=debian-10 --image-project=debian-cloud \
  --scopes=https://www.googleapis.com/auth/cloud-platform \
  --region=`echo $CLUSTER2ZONE | cut -f1-2 -d '-'` -q \
  --metadata-from-file "startup-script=$(dirname "$0")/startup-script.sh" \
  --metadata docker-image="$IMAGE_LOCATION",docker-entrypoint="$IMAGE_ENTRYPOINT"

echo -e "Creating ${yellow}Managed Instance Groups${nc}."
gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --zone ${CLUSTER1ZONE} --base-instance-name ${CLUSTER}-vm-${CLUSTER1ZONE} \
  --size=1 --template=${CLUSTER}-tpl-${CLUSTER1ZONE}
gcloud compute instance-groups managed create ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --zone ${CLUSTER2ZONE} --base-instance-name ${CLUSTER}-vm-${CLUSTER2ZONE} \
  --size=1 --template=${CLUSTER}-tpl-${CLUSTER2ZONE}

echo -e "Setting ${yellow}Instance Group Named Ports${nc}."
gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --named-ports "grpc:8080" --zone ${CLUSTER1ZONE}
gcloud compute instance-groups set-named-ports ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --named-ports "grpc:8080" --zone ${CLUSTER2ZONE}

echo -e "Creating ${yellow}Compute Engine Backend Service${nc}."
gcloud compute backend-services create ${CLUSTER}-checkoutservice-vm-bes --global \
  --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --connection-draining-timeout=30s --port-name=grpc \
  --health-checks ${CLUSTER}-grpc-hc  --protocol grpc

echo -e "Adding ${yellow}Instance Groups to Backend Service${nc}."
gcloud compute backend-services add-backend ${CLUSTER}-checkoutservice-vm-bes \
  --instance-group ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --instance-group-zone ${CLUSTER1ZONE} \
  --global
gcloud compute backend-services add-backend ${CLUSTER}-checkoutservice-vm-bes \
  --instance-group ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --instance-group-zone ${CLUSTER2ZONE} \
  --global

echo -e "Adding ${yellow}Path Matcher for VMs to the URL Map ${CLUSTER}-url-map${nc}."
gcloud compute url-maps remove-path-matcher ${CLUSTER}-url-map \
  --path-matcher-name pathmatch-checkoutservice
gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-checkoutservice-vm-bes \
  --path-matcher-name pathmatch-checkoutservice --new-hosts="checkoutservice:8080"

echo -e "${yellow}Script Complete${nc}."
printf "Note: It can take ten minutes to configure. Please be patient.\nAccess the application from: https://$(gcloud endpoints services list --format="value(serviceName)")\n"
