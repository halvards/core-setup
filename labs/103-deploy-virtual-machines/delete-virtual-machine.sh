#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
CLUSTER="${CLUSTER:-hipsterdemo}"
IFS=$'\n'
yellow='\033[1;33m'
nc='\033[0m' # No Color
echo -e "Deleting ${yellow}Compute Engine path matcher from URL Map${nc}."
gcloud compute url-maps remove-path-matcher ${CLUSTER}-url-map \
  --path-matcher-name pathmatch-checkoutservice
gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-checkoutservice-bes \
  --path-matcher-name pathmatch-checkoutservice --new-hosts="checkoutservice:8080"
echo -e "Deleting ${yellow}Backend Services${nc}."
for bes in $(gcloud compute backend-services list --format="value(name)" --filter="name~'^${CLUSTER}.*-vm-bes$'")
do
  gcloud compute backend-services delete -q --global $bes
done
echo -e "Deleting ${yellow}Compute Engine Managed Instance Groups${nc}."
for mig in $(gcloud compute instance-groups managed list --format="value(name,zone)" --filter="name~'^${CLUSTER}-mig\-'")
do
  gcloud compute instance-groups managed delete -q $(echo $mig | cut -f 1) --zone=$(echo $mig | cut -f 2)
done
echo -e "Deleting ${yellow}Instance Templates${nc}."
for template in $(gcloud compute instance-templates list --format="value(name)" --filter="name~'\-tpl\-'")
do
  gcloud compute instance-templates delete -q $template
done
echo -e "${yellow}Script Complete${nc}."