#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-microsvcdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
VM_IMAGE_PROJECT="${VM_IMAGE_PROJECT:-debian-cloud}"
VM_IMAGE_FAMILY="${VM_IMAGE_FAMILY:-debian-10}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Creating ${yellow}VM Instance for Chrome Remote${nc}."
gcloud compute instances create ${CLUSTER}-vm-client \
  --image-project $VM_IMAGE_PROJECT --image-family $VM_IMAGE_FAMILY \
  --zone $CLUSTER1ZONE \
  --metadata-from-file "startup-script=$(dirname "$0")/startup-script.sh"

echo -e "${yellow}Script Complete${nc}. Please see ${yellow}README.md${nc} for next steps (under ${yellow}Configure client VM${nc})."