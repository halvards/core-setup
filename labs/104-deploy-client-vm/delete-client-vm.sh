#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER="${CLUSTER:-hipsterdemo}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Deleting ${yellow}VM Instance for Chrome Remote${nc}."
gcloud compute instances delete -q ${CLUSTER}-vm-client --zone $CLUSTER1ZONE
echo -e "${yellow}Script Complete${nc}."