#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID=${PROJECT_ID:-$(gcloud config get-value project)}
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Deleting ${yellow}endpoint config selector${nc}."
gcloud alpha network-services endpoint-config-selectors delete ${CLUSTER}-ecs --location global --quiet

echo -e "Deleting ${yellow}authorization policy${nc}."
gcloud alpha network-security authorization-policies delete ${CLUSTER}-authz-policy --location global --quiet

echo -e "Deleting ${yellow}Server TLS policy${nc}."
gcloud alpha network-security server-tls-policies delete ${CLUSTER}-server-mtls-policy --location global --quiet

echo -e "Removing ${yellow}client policy from backend services${nc}."
SERVICES="cartservice productcatalogservice currencyservice paymentservice shippingservice shippingservice-enhanced emailservice recommendationservice adservice"
for service in $SERVICES
do
  echo -e "Deleting ${yellow}client policy${nc} from ${service}."
  gcloud beta compute backend-services export ${CLUSTER}-${service}-bes --global \
    --destination=backend-service.yaml

  sed -i "s/securitySettings:.*/securitySettings: {}/g" ./backend-service.yaml
  sed -i "s/  clientTlsPolicy:.*//g" ./backend-service.yaml
  sed -i "s/  subjectAltNames:.*//g" ./backend-service.yaml
  sed -i "s/  - spiffe:.*//g" ./backend-service.yaml

  gcloud beta compute backend-services import ${CLUSTER}-${service}-bes --global \
    --source=backend-service.yaml --quiet

  rm ./backend-service.yaml
done

echo -e "Deleting ${yellow}Client TLS policy${nc}."
gcloud alpha network-security client-tls-policies delete ${CLUSTER}-client-mtls-policy --location global --quiet

echo -e "Switch back to mixed topology by updating ${yellow}URL Maps${nc}"
gcloud compute url-maps remove-path-matcher ${CLUSTER}-url-map \
  --path-matcher-name pathmatch-checkoutservice
gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-checkoutservice-vm-bes \
  --path-matcher-name pathmatch-checkoutservice --new-hosts="checkoutservice:8080"
echo -e "Deleting ${yellow}Private CA${nc}."
IFS=$'\n'
for privateca in $(gcloud beta privateca roots list --format="get(name,state)" | grep ${CLUSTER} | grep "ENABLED$")
do
  gcloud beta privateca roots disable -q $(echo $privateca | cut -f 1)
  gcloud beta privateca roots delete -q $(echo $privateca | cut -f 1) --ignore-active-certificates
done
unset IFS
echo -e "${yellow}Script Complete${nc}."