#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Sample script for creating a cluster for deploying microservices app
# to demo Cloud Run for Anthos
#
# PREREQUISITES
# =============
# 1) Ensure you have created a project with billing enabled
# https://support.google.com/googleapi/answer/6251787?hl=en
#

CLUSTER="${CLUSTER:-microsvcdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
NETWORKNAME="${NETWORKNAME:-default}"
NODES="${NODES:-3}"
MAXNODES="${MAXNODES:-6}"
MACHINE="${MACHINE:-e2-standard-4}"
CHANNEL="${CHANNEL:-rapid}"
#CLUSTER1MAXVER=`gcloud container get-server-config --zone $CLUSTER1ZONE --format="value(channels[0].validVersions[0])"`
#CLUSTER2MAXVER=`gcloud container get-server-config --zone $CLUSTER2ZONE --format="value(channels[0].validVersions[0])"`
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
PROJECT_NUM="${PROJECT_NUM:-$(gcloud projects describe $PROJECT_ID --format="value(projectNumber)")}"
CA_NAME="${CA_NAME:-$CLUSTER}"
CA_LOCATION="us-central1"
CA_URL="//privateca.googleapis.com/projects/${PROJECT_ID}/locations/${CA_LOCATION}/certificateAuthorities/${CA_NAME}"
SA_NAME="${SA_NAME:-${CLUSTER}sa}"
SA="${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
GKE_SA="service-${PROJECT_NUM}@container-engine-robot.iam.gserviceaccount.com"
GCE_SA=$(gcloud iam service-accounts list --format='value(email)' --filter='displayName:Compute Engine default service account')
GSA_EMAIL=$(gcloud iam service-accounts list --format='value(email)' --filter='displayName:Compute Engine default service account')
ME=$(gcloud config get-value account)
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Creating ${yellow}Firewall Rules${nc}."
gcloud compute firewall-rules create ${CLUSTER}-hc-${NETWORKNAME} \
  --network=$NETWORKNAME --action=ALLOW --rules=tcp \
  --source-ranges=130.211.0.0/22,35.191.0.0/16
gcloud compute firewall-rules create ${CLUSTER}-int-${NETWORKNAME} \
  --network=default --action=ALLOW --rules=tcp \
  --source-ranges=10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
echo -e "Creating ${yellow}Pub/Sub Topics${nc}."
TOPICS=(adservice cartservice checkoutservice currencyservice emailservice frontend managementloggingservice managementservice paymentservice productcatalogservice recommendationservice shippingservice)
for topic in ${TOPICS[@]}
do
  gcloud pubsub topics create $topic
done
echo -e "Creating ${yellow}GKE Clusters${nc}."
gcloud beta container clusters create "${CLUSTER}-${CLUSTER1ZONE}" \
  --release-channel "${CHANNEL}" \
  --zone "${CLUSTER1ZONE}" \
  --num-nodes "${NODES}" \
  --machine-type "${MACHINE}" \
  --scopes=cloud-platform \
  --workload-pool=${PROJECT_ID}.svc.id.goog \
  --enable-workload-certificates \
  --workload-metadata=GKE_METADATA \
  --enable-ip-alias &
gcloud beta container clusters create "${CLUSTER}-${CLUSTER2ZONE}" \
  --release-channel "${CHANNEL}" \
  --zone "${CLUSTER2ZONE}" \
  --num-nodes "${NODES}" \
  --machine-type "${MACHINE}" \
  --scopes=https://www.googleapis.com/auth/cloud-platform \
  --workload-pool=${PROJECT_ID}.svc.id.goog \
  --enable-workload-certificates \
  --workload-metadata=GKE_METADATA \
  --enable-ip-alias
wait
echo -e "Registering with ${yellow}GKE Hub${nc}."
gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER1ZONE} \
--gke-cluster=${CLUSTER1ZONE}/${CLUSTER}-${CLUSTER1ZONE} \
--enable-workload-identity \
--manifest-output-file=${CLUSTER}-${CLUSTER1ZONE}-manifest.yaml
kubectl apply -f ${CLUSTER}-${CLUSTER1ZONE}-manifest.yaml
gcloud beta container hub memberships register ${CLUSTER}-${CLUSTER2ZONE} \
--gke-cluster=${CLUSTER2ZONE}/${CLUSTER}-${CLUSTER2ZONE} \
--enable-workload-identity \
--manifest-output-file=${CLUSTER}-${CLUSTER2ZONE}-manifest.yaml
kubectl apply -f ${CLUSTER}-${CLUSTER2ZONE}-manifest.yaml
rm ${CLUSTER}-${CLUSTER1ZONE}-manifest.yaml ${CLUSTER}-${CLUSTER2ZONE}-manifest.yaml
echo -e "Retrieving ${yellow}GKE Cluster Credentials${nc}."
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER1ZONE}" \
  --zone "${CLUSTER1ZONE}"
export CONTEXT_CLUSTER_1=$(kubectl config current-context)
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER2ZONE}" \
  --zone "${CLUSTER2ZONE}"
export CONTEXT_CLUSTER_2=$(kubectl config current-context)

echo -e "Create ${yellow}GKE service accounts${nc}."
ACCOUNTS="adservice cartservice checkoutservice currencyservice emailservice frontend paymentservice productcatalogservice recommendationservice shippingservice shippingservice-enhanced"
for account in $ACCOUNTS
do
  kubectl create serviceaccount --namespace "default" --cluster ${CONTEXT_CLUSTER_1} $account
  kubectl create serviceaccount --namespace "default" --cluster ${CONTEXT_CLUSTER_2} $account
done

echo -e "Setting up ${yellow}Service Account Bindings${nc}."
for account in $ACCOUNTS
do
  gcloud iam service-accounts add-iam-policy-binding  \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[default/$account]" ${GSA_EMAIL}
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[default/$account]" \
    --role roles/trafficdirector.client
  kubectl annotate serviceaccount \
    --namespace default $account \
    iam.gke.io/gcp-service-account=${GSA_EMAIL} \
    --cluster ${CONTEXT_CLUSTER_1}
  kubectl annotate serviceaccount \
    --namespace default $account \
    iam.gke.io/gcp-service-account=${GSA_EMAIL} \
    --cluster ${CONTEXT_CLUSTER_2}
done

gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:${PROJECT_ID}.svc.id.goog[default/default]" ${GSA_EMAIL}
kubectl annotate serviceaccount \
  --namespace default default \
  iam.gke.io/gcp-service-account=${GSA_EMAIL} \
  --cluster ${CONTEXT_CLUSTER_1}
kubectl annotate serviceaccount \
  --namespace default default \
  iam.gke.io/gcp-service-account=${GSA_EMAIL} \
  --cluster ${CONTEXT_CLUSTER_2}

echo -e "Retrieving ${yellow}Traffic Director Auto-Injector Package${nc}."
wget -O- https://storage.googleapis.com/traffic-director/td-sidecar-injector.tgz | tar xzv
pushd td-sidecar-injector
echo -e "Modifying ${yellow}specs/01-configmap.yaml${nc}."
sed -i "s/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:.*/TRAFFICDIRECTOR_GCP_PROJECT_NUMBER:\ \"$PROJECT_NUM\"/g" specs/01-configmap.yaml
sed -i "s/TRAFFICDIRECTOR_NETWORK_NAME:.*/TRAFFICDIRECTOR_NETWORK_NAME:\ \"$NETWORKNAME\"/g" specs/01-configmap.yaml

echo -e "Creating ${yellow}Certificates${nc}."
CN=istio-sidecar-injector.istio-control.svc
openssl req \
  -x509 \
  -newkey rsa:4096 \
  -keyout key.pem \
  -out cert.pem \
  -days 365 \
  -nodes \
  -subj "/CN=${CN}"

cp cert.pem ca-cert.pem

echo -e "Creating ${yellow}Namespaces for GKE Secrets${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/00-namespaces.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/00-namespaces.yaml

echo -e "Creating ${yellow}GKE Cluster Secrets${nc}."
kubectl create --cluster ${CONTEXT_CLUSTER_1} secret generic istio-sidecar-injector -n istio-control \
  --from-file=key.pem \
  --from-file=cert.pem \
  --from-file=ca-cert.pem
kubectl create --cluster ${CONTEXT_CLUSTER_2} secret generic istio-sidecar-injector -n istio-control \
  --from-file=key.pem \
  --from-file=cert.pem \
  --from-file=ca-cert.pem

CA_BUNDLE=$(cat ca-cert.pem | base64 | tr -d '\n')
sed -i "s/caBundle:.*/caBundle:\ ${CA_BUNDLE}/g" specs/02-injector.yaml

echo -e "Deploying ${yellow}Sidecar Injectors to GKE Clusters${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f specs/
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -f specs/

echo -e "Enabling ${yellow}Auto-injection in default namespace${nc}."
kubectl label --cluster ${CONTEXT_CLUSTER_1} namespace default istio-injection=enabled
kubectl label --cluster ${CONTEXT_CLUSTER_2} namespace default istio-injection=enabled
popd

echo -e "Creating ${yellow}TCP Backend Services${nc} and setting ${yellow}localityLbPolicy to ORIGINAL_DESTINATION${nc}."
gcloud compute backend-services create ${CLUSTER}-default-bes --global --load-balancing-scheme INTERNAL_SELF_MANAGED --protocol=tcp
curl -H "Authorization: Bearer $(gcloud auth print-access-token)" -H "Content-Type: application/json" --data \
  '{"localityLbPolicy": "ORIGINAL_DESTINATION" }' \
  -X PATCH "https://compute.googleapis.com/compute/v1/projects/${PROJECT_ID}/global/backendServices/${CLUSTER}-default-bes"

echo -e "Creating ${yellow}TCP Target Proxy for default traffic${nc}."
sleep 30s
gcloud compute target-tcp-proxies create ${CLUSTER}-tcp-proxy --backend-service=${CLUSTER}-default-bes
gcloud compute forwarding-rules create ${CLUSTER}-td-fr-443 \
  --global --address=0.0.0.0 \
  --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --target-tcp-proxy=${CLUSTER}-tcp-proxy \
  --ports 443 --network $NETWORKNAME

echo -e "Creating ${yellow}TCP and gRPC Health Checks${nc}."
gcloud compute health-checks create tcp ${CLUSTER}-redis-hc --port=6379 --global
gcloud compute health-checks create grpc ${CLUSTER}-grpc-hc \
  --use-serving-port --global --check-interval=1 \
  --healthy-threshold=2 --unhealthy-threshold=3 --timeout=1

echo -e "Creating ${yellow}Backend Service for Redis service${nc}."
gcloud compute backend-services create ${CLUSTER}-redis-bes \
  --global --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --health-checks=${CLUSTER}-redis-hc --protocol=TCP \
  --global-health-checks
sleep 30s
echo -e "Creating ${yellow}Target TCP proxy for Redis service${nc}."
gcloud compute target-tcp-proxies create ${CLUSTER}-redis-proxy \
  --backend-service=${CLUSTER}-redis-bes

echo -e "Creating ${yellow}TCP Forwarding Rule for Redis service${nc}."
gcloud compute forwarding-rules create ${CLUSTER}-redis-fr \
  --global --address=0.0.0.0 \
  --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --target-tcp-proxy=${CLUSTER}-redis-proxy \
  --ports 6379 --network $NETWORKNAME

echo -e "Creating ${yellow}gRPC Backend Services${nc}."
SERVICES="cartservice productcatalogservice currencyservice paymentservice shippingservice emailservice recommendationservice adservice checkoutservice"
for service in $SERVICES
do
  gcloud compute backend-services create ${CLUSTER}-${service}-bes \
    --global --health-checks ${CLUSTER}-grpc-hc  --protocol grpc \
    --load-balancing-scheme INTERNAL_SELF_MANAGED
done

echo -e "${yellow}Script Complete${nc}."
