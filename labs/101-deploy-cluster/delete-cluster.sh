#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
NETWORKNAME="${NETWORKNAME:-default}"
CA_NAME="${CA_NAME:-$CLUSTER}"
CA_LOCATION="${CA_LOCATION:-us-central1}"
yellow='\033[1;33m'
nc='\033[0m' # No Color
echo -e "Deleting ${yellow}Forwarding Rules${nc}."
for fwdrule in $(gcloud compute forwarding-rules list --format="value(name)" --filter="name~'${CLUSTER}-td-fr|${CLUSTER}-ext-fr|${CLUSTER}-td-tcp|${CLUSTER}-redis-fr'")
do
  gcloud compute forwarding-rules delete -q --global $fwdrule
done
echo -e "Deleting ${yellow}Target TCP Proxies${nc}."
for targets in $(gcloud compute target-tcp-proxies list --format="value(name)" --filter="name~'${CLUSTER}-tcp-proxy|${CLUSTER}-redis-proxy'")
do
  gcloud compute target-tcp-proxies delete -q $targets
done
echo -e "Deleting ${yellow}Backend Services${nc}."
for backend_services in $(gcloud compute backend-services list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute backend-services delete --global -q $backend_services
done

echo -e "Deleting ${yellow}Target TCP Proxies${nc}."
for targets in $(gcloud compute target-tcp-proxies list --format="value(name)" --filter="name~'${CLUSTER}-tcp-proxy|${CLUSTER}-redis-proxy'")
do
  gcloud compute target-tcp-proxies delete -q $targets
done
echo -e "Deleting ${yellow}Health Checks${nc}."
for health_checks in $(gcloud compute health-checks list --format="value(name)" --filter="name~'${CLUSTER}-'")
do
  gcloud compute health-checks delete --global -q $health_checks
done

echo -e "Deleting ${yellow}Service Account${nc}."
gcloud iam service-accounts delete ${SA} -q
echo -e "Deleting ${yellow}GKE Clusters in zones ${CLUSTER1ZONE}${nc} and ${yellow}${CLUSTER2ZONE}${nc}."
gcloud container clusters delete "${CLUSTER}-${CLUSTER1ZONE}" --zone "${CLUSTER1ZONE}" -q &
gcloud container clusters delete "${CLUSTER}-${CLUSTER2ZONE}" --zone "${CLUSTER2ZONE}" -q
echo -e "Deleting ${yellow}GKE Hub Entries${nc}."
gcloud container hub memberships delete ${CLUSTER}-${CLUSTER1ZONE} -q
gcloud container hub memberships delete ${CLUSTER}-${CLUSTER2ZONE} -q
echo -e "Deleting Pub/Sub Topics and Subscriptions${nc}."
TOPICS="adservice cartservice checkoutservice currencyservice emailservice frontend managementloggingservice managementservice paymentservice productcatalogservice recommendationservice shippingservice"
for topic in $TOPICS
do
  gcloud pubsub topics delete $topic -q
done
for sub in $(gcloud pubsub subscriptions list --format="value(name)" --filter="name~'$(echo $TOPICS | sed s/' '/'|'/g)'")
do
  gcloud pubsub subscriptions delete $sub -q
done
echo -e "Deleting ${yellow}Firewall Rules${nc}."
gcloud compute firewall-rules delete ${CLUSTER}-hc-${NETWORKNAME} -q
gcloud compute firewall-rules delete ${CLUSTER}-int-${NETWORKNAME} -q
echo -e "Removing ${yellow}Project-Specific Data${nc}."
rm -rf td-sidecar-injector
echo -e "${yellow}Script Complete${nc}."