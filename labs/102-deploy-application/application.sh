#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#!/bin/bash

#Create all the backend services:
#TODO: Need to mention raise quota for backend services (9 max)
CLUSTER="${CLUSTER:-microsvcdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
NETWORKNAME="${NETWORKNAME:-default}"
PROJECT_ID=${PROJECT_ID:-$(gcloud config get-value project)}
SERVICES=${SERVICES:-(adservice cartservice currencyservice emailservice productcatalogservice paymentservice shippingservice recommendationservice)}
yellow='\033[1;33m'
nc='\033[0m' # No Color

# Requirement for Traffic Director TCP backend
#!/bin/bash
if ! (( $(gcloud version --format="value('Google Cloud SDK')" | cut -d'.' -f1) >= 320 )) ; then
    echo "Please update Google Cloud SDK to >= 320.0.0"
    exit 1
fi

echo -e "Retrieving ${yellow}GKE Cluster Credentials${nc}."
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER1ZONE}" --zone "${CLUSTER1ZONE}"
export CONTEXT_CLUSTER_1=$(kubectl config current-context)
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER2ZONE}" --zone "${CLUSTER2ZONE}"
export CONTEXT_CLUSTER_2=$(kubectl config current-context)

echo -e "Retrieving ${yellow}microservices-demo manifest${nc}."
wget -O labs/102-deploy-application/kubernetes-manifests.yaml https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/master/release/kubernetes-manifests.yaml

echo -e "Applying manifests to ${yellow}${CONTEXT_CLUSTER_1}${nc} and ${yellow}${CONTEXT_CLUSTER_2}${nc}."
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -k ./labs/102-deploy-application/
kubectl apply --cluster ${CONTEXT_CLUSTER_1} -f ./labs/102-deploy-application/management-service.yaml
kubectl apply --cluster ${CONTEXT_CLUSTER_2} -k ./labs/102-deploy-application/

echo -e "Adding backends to the ${yellow}Backend Services${nc}."
sleep 30s
for service in ${SERVICES[@]}
do
  gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
    --global \
    --network-endpoint-group "${service}-neg" \
    --network-endpoint-group-zone $CLUSTER1ZONE \
    --balancing-mode RATE \
    --max-rate-per-endpoint 5
  gcloud compute backend-services add-backend ${CLUSTER}-${service}-bes \
    --global \
    --network-endpoint-group "${service}-neg" \
    --network-endpoint-group-zone $CLUSTER2ZONE \
    --balancing-mode RATE \
    --max-rate-per-endpoint 5
done
gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
  --network-endpoint-group redis-neg \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode CONNECTION --max-connections-per-endpoint=250
gcloud compute backend-services add-backend ${CLUSTER}-redis-bes --global \
  --network-endpoint-group redis-neg \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode CONNECTION --max-connections-per-endpoint=250

echo -e "Creating ${yellow}gRPC URL Map${nc}."
gcloud compute url-maps create ${CLUSTER}-url-map \
  --default-service ${CLUSTER}-cartservice-bes --global

echo -e "Adding ${yellow}URL Map Path Matchers${nc}."
for service in $SERVICES
do
  gcloud compute url-maps add-path-matcher ${CLUSTER}-url-map \
    --default-service ${CLUSTER}-${service}-bes \
    --path-matcher-name pathmatch-${service} \
    --new-hosts="${service}:8080"
done

echo -e "Creating ${yellow}gRPC Target Proxy${nc}."
gcloud compute target-grpc-proxies create ${CLUSTER}-grpc-proxy \
    --url-map ${CLUSTER}-url-map

echo -e "Creating ${yellow}gRPC Forwarding Rule${nc}."
gcloud compute forwarding-rules create ${CLUSTER}-td-fr \
  --global --address=0.0.0.0 \
  --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --target-grpc-proxy=${CLUSTER}-grpc-proxy \
  --ports 8080 --network $NETWORKNAME
gcloud compute forwarding-rules create ${CLUSTER}-td-fr-vip \
  --global --address=10.254.254.101 \
  --load-balancing-scheme=INTERNAL_SELF_MANAGED \
  --target-grpc-proxy=${CLUSTER}-grpc-proxy \
  --ports 8080 --network $NETWORKNAME

# Start Multi cluster manual ingress
echo -e "Reserving ${yellow}IPv4 address for front-end service${nc}."
gcloud compute addresses create ${CLUSTER}-ext-vip \
  --ip-version=IPV4 \
  --global

echo -e "Creating ${yellow}Managed Certificate${nc}."
#Need randomization for the cloud endpoints. After deletion it's kept for 30 days so you can't re-deploy
RND=$(cat /dev/urandom | tr -dc "a-z" | fold -w 4 | head -n 1)
gcloud compute ssl-certificates create ${CLUSTER}-ext-cert \
  --domains=${CLUSTER}-${RND}.endpoints.${PROJECT_ID}.cloud.goog \
  --global

echo -e "Creating ${yellow}External Health Checks${nc}."
gcloud compute health-checks create http ${CLUSTER}-ext-hc \
  --use-serving-port --check-interval=1 --healthy-threshold=2 --unhealthy-threshold=3 --timeout=1 \
  --request-path "/_healthz"
gcloud compute health-checks create http ${CLUSTER}-managementservice-hc \
  --use-serving-port --check-interval=1 --healthy-threshold=2 --unhealthy-threshold=3 --timeout=1 \
  --request-path "/api/health"

echo -e "Creating ${yellow}External Backend Services${nc}."
gcloud compute backend-services create ${CLUSTER}-ext-bes \
  --protocol HTTP \
  --health-checks ${CLUSTER}-ext-hc \
  --global
gcloud compute backend-services create ${CLUSTER}-managementservice-bes \
  --protocol HTTP \
  --health-checks ${CLUSTER}-managementservice-hc \
  --global

echo -e "Creating ${yellow}External URL Map${nc}."
gcloud compute url-maps create ${CLUSTER}-ext-map \
  --default-service ${CLUSTER}-ext-bes
gcloud compute url-maps add-path-matcher ${CLUSTER}-ext-map \
  --default-service ${CLUSTER}-ext-bes \
  --path-matcher-name control-api \
  --path-rules "/api/control=${CLUSTER}-managementservice-bes,/api/logging/*=${CLUSTER}-managementservice-bes"

echo -e "Creating ${yellow}External Target HTTPS Proxy${nc}."
gcloud compute target-https-proxies create ${CLUSTER}-ext-proxy \
  --url-map ${CLUSTER}-ext-map --ssl-certificates=${CLUSTER}-ext-cert \
  --quic-override=ENABLE

echo -e "Creating ${yellow}External Forwarding Rule${nc}."
gcloud compute forwarding-rules create ${CLUSTER}-ext-fr \
  --address=${CLUSTER}-ext-vip \
  --global \
  --target-https-proxy=${CLUSTER}-ext-proxy \
  --ports=443

echo -e "Creating ${yellow}DNS for Cloud Endpoints${nc}."
cat <<EOF > dns-spec.yaml
swagger: "2.0"
info:
  description: "Cloud Endpoints DNS"
  title: "Cloud Endpoints DNS"
  version: "1.0.0"
paths: {}
host: "${CLUSTER}-${RND}.endpoints.${PROJECT_ID}.cloud.goog"
x-google-endpoints:
- name: "${CLUSTER}-${RND}.endpoints.${PROJECT_ID}.cloud.goog"
  target: "$(gcloud compute forwarding-rules describe ${CLUSTER}-ext-fr --global --format="value(IPAddress)")"
EOF
gcloud endpoints services deploy dns-spec.yaml

echo -e "Adding ${yellow}External Services to Backend Service${nc}."
gcloud compute backend-services add-backend ${CLUSTER}-ext-bes --global \
  --network-endpoint-group "frontend-ext-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-ext-bes --global \
  --network-endpoint-group "frontend-ext-neg" \
  --network-endpoint-group-zone $CLUSTER2ZONE \
  --balancing-mode RATE --max-rate-per-endpoint 5
gcloud compute backend-services add-backend ${CLUSTER}-managementservice-bes --global \
  --network-endpoint-group "managementservice-neg" \
  --network-endpoint-group-zone $CLUSTER1ZONE \
  --balancing-mode RATE --max-rate-per-endpoint 5
echo -e "${yellow}Script Complete${nc}."
