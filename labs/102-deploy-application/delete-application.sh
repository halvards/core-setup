#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
IFS=$'\n'
CLUSTER="${CLUSTER:-hipsterdemo}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
yellow='\033[1;33m'
nc='\033[0m' # No Color


echo -e "Removing ${yellow}Forwarding Rule${nc}."
gcloud compute forwarding-rules delete -q --global ${CLUSTER}-ext-fr
gcloud compute forwarding-rules delete -q --global ${CLUSTER}-td-fr
gcloud compute forwarding-rules delete -q --global ${CLUSTER}-td-fr-vip


echo -e "Deleting ${yellow}External Target HTTPS Proxy${nc}."
gcloud compute target-https-proxies delete -q --global ${CLUSTER}-ext-proxy
echo -e "Deleting ${yellow}Target gRPC Proxies${nc}."
for targets in $(gcloud compute target-grpc-proxies list --format="value(name)" --filter="name~'${CLUSTER}-grpc-proxy'")
do
  gcloud compute target-grpc-proxies delete -q $targets
done

echo -e "Deleting ${yellow}URL Maps${nc}."
for url_maps in $(gcloud compute url-maps list --format="value(name)" --filter="name~'${CLUSTER}-ext-map|${CLUSTER}-url-map'")
do
  gcloud compute url-maps delete -q --global  $url_maps
done

echo -e "Removing ${yellow}External Backend-service${nc}."
gcloud compute backend-services delete ${CLUSTER}-ext-bes --global -q
gcloud compute backend-services delete ${CLUSTER}-managementservice-bes --global -q

echo -e "Deleting ${yellow}External Target HTTPS Proxy${nc}."
gcloud compute target-https-proxies delete -q --global ${CLUSTER}-ext-proxy

gcloud compute health-checks delete -q ${CLUSTER}-ext-hc
gcloud compute health-checks delete -q ${CLUSTER}-managementservice-hc
echo -e "Deleting ${yellow}Cloud Endpoints DNS${nc}."
for endpoint in $(gcloud endpoints services list --format="value(serviceName)" --filter="serviceName~'^${CLUSTER}.*'")
do
  gcloud endpoints services delete -q $(echo $endpoint)
done
echo -e "Deleting ${yellow}SSL Certificate${nc}."
gcloud compute ssl-certificates delete ${CLUSTER}-ext-cert --global -q
echo -e "Deleting ${yellow}GKE Services, Service Accounts and Deployments${nc}."
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER1ZONE}" --zone "${CLUSTER1ZONE}"
kubectl delete services --all
kubectl delete deployments --all
kubectl delete serviceaccounts --all
gcloud container clusters get-credentials "${CLUSTER}-${CLUSTER2ZONE}" --zone "${CLUSTER2ZONE}"
kubectl delete services --all
kubectl delete deployments --all
kubectl delete serviceaccounts --all
echo -e "Removing NEGs from ${yellow}Backend Services${nc}."
for backend in $(gcloud compute backend-services list --format="value(name,backends[].group)" --filter="name~'^${CLUSTER}-'")
do
  for neg in $(echo $backend | cut -f 2 | sed "s/,/\n/g")
  do
    gcloud compute backend-services remove-backend -q $(echo $backend | cut -f 1) --network-endpoint-group=$(echo $neg | cut -d "/" -f 3) --global --network-endpoint-group-zone=$(echo $neg | cut -d "/" -f 1) 
  done
done
echo -e "Deleting ${yellow}Network Endpoint Groups${nc}."
for neg in $(gcloud compute network-endpoint-groups list --format="value(name,zone)" --filter="name~'-neg.*'")
do
  gcloud compute network-endpoint-groups delete -q $(echo $neg | cut -f 1) --zone=$(echo $neg | cut -f 2)
done
# Multi cluster ingress delete
echo -e "Deleting ${yellow}Reserved IPv4 Address${nc}."
gcloud compute addresses delete ${CLUSTER}-ext-vip --global -q
echo -e "${yellow}Script Complete${nc}."