#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
NETWORKNAME="${NETWORKNAME:-default}"
CONTEXT_CLUSTER_1="gke_${PROJECT_ID}_${CLUSTER1ZONE}_${CLUSTER}-${CLUSTER1ZONE}"
CONTEXT_CLUSTER_2="gke_${PROJECT_ID}_${CLUSTER2ZONE}_${CLUSTER}-${CLUSTER2ZONE}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Enabling ${yellow}autoscaling on cluster pool${nc}."
gcloud container clusters update ${CLUSTER}-${CLUSTER1ZONE} --enable-autoscaling \
  --min-nodes 3 --max-nodes 10 --zone $CLUSTER1ZONE \
  --node-pool default-pool
gcloud container clusters update ${CLUSTER}-${CLUSTER2ZONE} --enable-autoscaling \
  --min-nodes 3 --max-nodes 10 --zone $CLUSTER2ZONE \
  --node-pool default-pool

echo -e "Enabling ${yellow}autoscaling on virtual machines${nc}."
gcloud compute instance-groups managed set-autoscaling ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --max-num-replicas 5 --zone $CLUSTER1ZONE
gcloud compute instance-groups managed set-autoscaling ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --max-num-replicas 5 --zone $CLUSTER2ZONE

echo -e "Applying ${yellow}HPA for core services${nc}."
kubectl apply --cluster $CONTEXT_CLUSTER_1 -f ./labs/107-deploy-autoscaling/autoscale.yaml
kubectl apply --cluster $CONTEXT_CLUSTER_2 -f ./labs/107-deploy-autoscaling/autoscale.yaml

echo -e "Disabling ${yellow}tracing, profiling, and debugging${nc}."

sed -i "s/        # - name: DISABLE_TRACING/        - name: DISABLE_TRACING/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        # - name: DISABLE_PROFILER/        - name: DISABLE_PROFILER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        # - name: DISABLE_DEBUGGER/        - name: DISABLE_DEBUGGER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        # - name: DISABLE_STATS/        - name: DISABLE_STATS/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        #   value: \"1\"/          value: \"1\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/          value: \"10\"/          value: \"50\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/            value: \"true\"/            value: \"false\"/g" ./labs/102-deploy-application/deployments-patch.yaml

sed -i "s/        # - name: DISABLE_TRACING/        - name: DISABLE_TRACING/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/        # - name: DISABLE_PROFILER/        - name: DISABLE_PROFILER/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/        # - name: DISABLE_STATS/        - name: DISABLE_STATS/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/          value: \"true\"/          value: \"false\"/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

kubectl apply --cluster $CONTEXT_CLUSTER_1 -k ./labs/102-deploy-application/
kubectl apply --cluster $CONTEXT_CLUSTER_2 -k ./labs/102-deploy-application/
kubectl apply --cluster $CONTEXT_CLUSTER_1 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
kubectl apply --cluster $CONTEXT_CLUSTER_2 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

sed -i "s/        - name: DISABLE_TRACING/        # - name: DISABLE_TRACING/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        - name: DISABLE_PROFILER/        # - name: DISABLE_PROFILER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        - name: DISABLE_DEBUGGER/        # - name: DISABLE_DEBUGGER/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/        - name: DISABLE_STATS/        # - name: DISABLE_STATS/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/          value: \"1\"/        #   value: \"1\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/          value: \"50\"/          value: \"10\"/g" ./labs/102-deploy-application/kubernetes-manifests.yaml
sed -i "s/            value: \"false\"/            value: \"true\"/g" ./labs/102-deploy-application/deployments-patch.yaml

sed -i "s/        - name: DISABLE_TRACING/        # - name: DISABLE_TRACING/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/        - name: DISABLE_PROFILER/        # - name: DISABLE_PROFILER/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/        - name: DISABLE_STATS        # - name: DISABLE_STATS/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
sed -i "s/          value: \"false\"/          value: \"true\"/g" ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

echo -e "Starting ${yellow}load balancer pods${nc}."
kubectl scale --cluster $CONTEXT_CLUSTER_1 deployment/loadgenerator --replicas 5
kubectl scale --cluster $CONTEXT_CLUSTER_2 deployment/loadgenerator --replicas 5

echo -e "${yellow}Script Complete${nc}."