#!/usr/bin/env bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CLUSTER="${CLUSTER:-hipsterdemo}"
PROJECT_ID="${PROJECT_ID:-$(gcloud config get-value project)}"
CLUSTER1ZONE="${ZONE:-us-central1-c}"
CLUSTER2ZONE="${ZONE:-asia-southeast1-b}"
NETWORKNAME="${NETWORKNAME:-default}"
CONTEXT_CLUSTER_1="gke_${PROJECT_ID}_${CLUSTER1ZONE}_${CLUSTER}-${CLUSTER1ZONE}"
CONTEXT_CLUSTER_2="gke_${PROJECT_ID}_${CLUSTER2ZONE}_${CLUSTER}-${CLUSTER2ZONE}"
yellow='\033[1;33m'
nc='\033[0m' # No Color

echo -e "Stopping ${yellow}load balancer pods${nc}."
kubectl scale --cluster $CONTEXT_CLUSTER_1 deployment/loadgenerator --replicas 0
kubectl scale --cluster $CONTEXT_CLUSTER_2 deployment/loadgenerator --replicas 0

echo -e "Enabling ${yellow}tracing, profiling, and debugging${nc}."
kubectl apply --cluster $CONTEXT_CLUSTER_1 -k ./labs/102-deploy-application/
kubectl apply --cluster $CONTEXT_CLUSTER_2 -k ./labs/102-deploy-application/
kubectl apply --cluster $CONTEXT_CLUSTER_1 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml
kubectl apply --cluster $CONTEXT_CLUSTER_2 -f ./labs/105-deploy-load-balancing/shippingservice-enhanced.yaml

echo -e "Removing ${yellow}HPA for core services${nc}."
kubectl delete --cluster $CONTEXT_CLUSTER_1 -f ./labs/107-deploy-autoscaling/autoscale.yaml
kubectl delete --cluster $CONTEXT_CLUSTER_2 -f ./labs/107-deploy-autoscaling/autoscale.yaml

echo -e "Force ${yellow}pod scale down${nc}."
SERVICES="cartservice productcatalogservice currencyservice paymentservice shippingservice shippingservice-enhanced emailservice recommendationservice adservice frontend checkoutservice"
for service in $SERVICES
do
  kubectl scale --cluster $CONTEXT_CLUSTER_1 "deployment/${service}" --replicas 1
  kubectl scale --cluster $CONTEXT_CLUSTER_2 "deployment/${service}" --replicas 1
done

echo -e "Disabling ${yellow}autoscaling on cluster pool${nc}."
gcloud container clusters update ${CLUSTER}-${CLUSTER1ZONE} --no-enable-autoscaling \
  --zone $CLUSTER1ZONE --node-pool default-pool
gcloud container clusters update ${CLUSTER}-${CLUSTER2ZONE} --no-enable-autoscaling \
  --zone $CLUSTER2ZONE --node-pool default-pool

echo -e "Disabling ${yellow}autoscaling on virtual machines${nc}."
gcloud compute instance-groups managed update-autoscaling ${CLUSTER}-mig-${CLUSTER1ZONE} \
  --mode off --zone $CLUSTER1ZONE
gcloud compute instance-groups managed update-autoscaling ${CLUSTER}-mig-${CLUSTER2ZONE} \
  --mode off --zone $CLUSTER2ZONE

echo -e "${yellow}Script Complete${nc}."